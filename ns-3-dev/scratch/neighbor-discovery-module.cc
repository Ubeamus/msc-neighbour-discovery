/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2005,2006 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Johannes Loeffler <johannes-loeffler@posteo.de>
 */

#include "ns3/wifi-net-device.h"
#include "ns3/yans-wifi-channel.h"
#include "ns3/yans-wifi-phy.h"
#include "ns3/propagation-loss-model.h"
#include "ns3/propagation-delay-model.h"
#include "ns3/error-rate-model.h"
#include "ns3/yans-error-rate-model.h"
#include "ns3/ptr.h"
#include "ns3/log.h"
#include "ns3/mobility-model.h"
#include "ns3/constant-position-mobility-model.h"
#include "ns3/packet.h"
#include "ns3/simulator.h"
#include "ns3/nstime.h"
#include "ns3/command-line.h"
#include "ns3/flow-id-tag.h"
#include "ns3/wifi-tx-vector.h"
#include "ns3/cosine-antenna-model.h"
#include "ns3/wifi-helper.h"
#include "ns3/neighbour-discovery.h"

#include <iostream>
#include <algorithm>
#include <stdlib.h>     /* srand, rand */
#include <stdio.h>      /* printf, NULL */
#include <fstream>
#include <sstream>		/*for std_to_string bug*/


// Patch for std::to_string
namespace patch
{
    template < typename T > std::string to_string( const T& n )
    {
        std::ostringstream stm ;
        stm << n ;
        return stm.str() ;
    }
}

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("NeighbourDiscoveryModule");


class AdaptiveNeighbourDiscovery
{
public:

  struct Input
  {
    Input ();
	std::string txMode;
	uint8_t txPowerLevel;
	uint32_t packetSize;
  };

  AdaptiveNeighbourDiscovery ();
  void Run ();
  void Initialze ();

  // Channel / Propagation
  Ptr<YansWifiChannel> channel;
  Ptr<FriisPropagationLossModel> log;
  Ptr<ErrorRateModel> error;

private:

  void createNodes(uint16_t number);
  void createChannelAndPropagationModel();

  void treeTest();

  void auswertungForSingleRun();
  void auswertungForAllRuns();
  void neighboursInRange();
  void resetValues();

  void percentageOfReceivedBeaconsPerRun();

  void printDisoveredNeighours();

  int currentRun;
  int runs;
  int nodes;
  int xMaxPos;
  int yMaxPos;
  int numberOfTimeSlotsBeforeStop;
  bool setup;
  bool printLayer;

  int printTimeslotStep;

  std::vector<double> averageNeighbours;

  std::vector<NeighbourDiscovery> nodesVector;

  // Evaluation variables
  int allRunsNeighbours;
  int allRunsTransmits;
  int allRunsReceives;
  int allRunsBeaconMessages;
  int allRunsInterference;

  // transmit / receive layer count
  std::vector<int> allTransmitperLayer;
  std::vector<int> allReceiveperLayer;

  std::vector<int> beaconTimestamp;
  std::vector<double> beaconDistance;

};

AdaptiveNeighbourDiscovery::AdaptiveNeighbourDiscovery(){
	currentRun = 1;
	runs = 5;
	nodes = 200;
	xMaxPos = 500;
	yMaxPos = 500;
	numberOfTimeSlotsBeforeStop = 16000;
	setup = false;

	printTimeslotStep = 1;

	// Evaluation variables
	allRunsNeighbours = 0;
	allRunsTransmits = 0;
	allRunsReceives = 0;
	allRunsBeaconMessages = 0;
	allRunsInterference = 0;

	for(int i=0; i < 4; i++){
		allTransmitperLayer.push_back(0);
		allReceiveperLayer.push_back(0);
	}
}
AdaptiveNeighbourDiscovery::Input::Input ()
  : txMode ("OfdmRate6Mbps"),
    txPowerLevel (0),
    packetSize (2304)
{
}

// Set global channel, propagation loss and error model
void
AdaptiveNeighbourDiscovery::createChannelAndPropagationModel(){
	// Channel
	channel = CreateObject<YansWifiChannel> ();
	channel->SetPropagationDelayModel (CreateObject<ConstantSpeedPropagationDelayModel> ());
	log = CreateObject<FriisPropagationLossModel> ();
	channel->SetPropagationLossModel (log);

	// Set 60 GHz for Propagation Loss Model
	log->SetFrequency(60000000000);

	// Error
	error = CreateObject<YansErrorRateModel> ();
}

void
AdaptiveNeighbourDiscovery::Initialze()
{
	NS_LOG_FUNCTION("AdaptiveNeighbourDiscovery::Initialze");

	// Activate Log Level

	//LogComponentEnable ("NeighbourDiscoveryModule", LOG_INFO);
	//LogComponentEnable ("YansWifiPhy", LOG_INFO);
	//LogComponentEnable ("YansWifiChannel", LOG_INFO);
	//LogComponentEnable ("WifiPhyStateHelper", LOG_INFO);
	//LogComponentEnable("NeighbourDiscovery", LOG_INFO);
	//LogComponentEnable ("Tree", LOG_INFO);

	//LogComponentEnable ("NeighbourDiscoveryModule", LOG_FUNCTION);
	//LogComponentEnable ("YansWifiPhy", LOG_FUNCTION);
	//LogComponentEnable ("YansWifiChannel", LOG_FUNCTION);
	//LogComponentEnable ("WifiPhyStateHelper", LOG_FUNCTION);
	//LogComponentEnable("NeighbourDiscovery", LOG_FUNCTION);
	//LogComponentEnable ("Tree", LOG_FUNCTION);

	//LogComponentEnable ("NeighbourDiscoveryModule", LOG_DEBUG);
	//LogComponentEnable ("YansWifiPhy", LOG_DEBUG);
	//LogComponentEnable ("YansWifiChannel", LOG_DEBUG);
	//LogComponentEnable ("WifiPhyStateHelper", LOG_DEBUG);
	//LogComponentEnable("NeighbourDiscovery", LOG_DEBUG);
	//LogComponentEnable ("Tree", LOG_DEBUG);

	// Randomize rand
	int init = 100 + currentRun;
	srand(init);

	// Create nodes
	createNodes(nodes);

	// Start simulation
	Simulator::Schedule(Seconds(0.0), &AdaptiveNeighbourDiscovery::Run, this);
	Simulator::Run();

}

void
AdaptiveNeighbourDiscovery::createNodes(uint16_t numberNodes){

	NS_LOG_FUNCTION("AdaptiveNeighbourDiscovery::createNodes");

	// Create global channel and error model
	createChannelAndPropagationModel();

	// Create nodes
	for (uint32_t i = 0; i < numberNodes; ++i)
	{
		NeighbourDiscovery nd;

		nd.CreateNode(channel, error, &nodesVector); 	// Create Node

		double xPos = rand() % xMaxPos;
		double yPos = rand() % yMaxPos;

		double offset = rand() % 360;

		nd.setXPos(xPos);
		nd.setYPos(yPos);

		nd.setAntennaOffset(offset);

		nd.setDirection(0);

		nd.setTimeSlot(0);

		nd.setBeamwidth(90);

		nd.setHandshake(true);

	    nd.setId(i);

	    nodesVector.push_back(nd);
	}

	// For test purposes
	if(setup){

	double beam = 90.0;

	nodesVector.at(0).setXPos(0.0); // x
	nodesVector.at(0).setYPos(0.0); // y
	nodesVector.at(0).setDirection(0); // orien. in degr.
	nodesVector.at(0).setBeamwidth(beam); // beamwidth

	nodesVector.at(1).setXPos(0.0); // x
	nodesVector.at(1).setYPos(0.0); // y
	nodesVector.at(1).setDirection(180); // orien. in degr.
	nodesVector.at(1).setBeamwidth(360); // beamwidth

	setup = false;

	}
}

void
AdaptiveNeighbourDiscovery::Run (){

	NS_LOG_FUNCTION("AdaptiveNeighbourDiscovery::Run (Simulator Time " << Simulator::Now() << ")");

	for (int i = 0; i < nodes; ++i){

		nodesVector.at(i).setMaxTimeSlot(numberOfTimeSlotsBeforeStop);

		// Adaptive
		nodesVector.at(i).initializeAdaptive();

		// nodesVector.at(i).initializeSweep(90);
	}

	// Delay needs to be high, so that the last sweep run is finished before nodes are deleted

	// --- This part is for evaluation printout ---
	Simulator::Schedule(Seconds(numberOfTimeSlotsBeforeStop + 500), &AdaptiveNeighbourDiscovery::auswertungForSingleRun, this);

	if(currentRun < runs){
		Simulator::Schedule(Seconds(numberOfTimeSlotsBeforeStop + 600), &AdaptiveNeighbourDiscovery::resetValues, this);
		Simulator::Schedule(Seconds(numberOfTimeSlotsBeforeStop + 700), &AdaptiveNeighbourDiscovery::Initialze, this);
		currentRun++;
	} else {
		Simulator::Schedule(Seconds(numberOfTimeSlotsBeforeStop + 800), &AdaptiveNeighbourDiscovery::auswertungForAllRuns, this);
	}
}

void AdaptiveNeighbourDiscovery::auswertungForSingleRun(){

	NS_LOG_FUNCTION("AdaptiveNeighbourDiscovery::auswertungForRun");

	// ---- Output number of all neighbours and all collisions

	int allNeighbours = 0;
	int allTransmits = 0;
	int allReceives = 0;
	int allBeaconMessages = 0;
	int allInterference = 0;

	for(int i=0; i< (int)nodesVector.size(); i++){
		allNeighbours += nodesVector.at(i).getDiscoveredNodesList().size();
		allTransmits += nodesVector.at(i).getTransmitCount();
		allReceives += nodesVector.at(i).getReceiveCount();
		allBeaconMessages += nodesVector.at(i).getBeaconMessagesCount();
		allInterference += nodesVector.at(i).getInterferenceCount();
	}

	NS_LOG_UNCOND("");
	NS_LOG_UNCOND("Run " << currentRun-1 << " #neighbours " << allNeighbours << " #transmit " << allTransmits <<
			" #receive " << allReceives << " #beaconMessages " << allBeaconMessages << " #interference " << allInterference);

	allRunsNeighbours += allNeighbours;
	allRunsTransmits += allTransmits;
	allRunsReceives += allReceives;
	allRunsBeaconMessages += allBeaconMessages;
	allRunsInterference += allInterference;

	// Print tree
	// nodesVector.at(0).getTransmitTree().printTree();

	// ---- Print out used layer and layer neighours are found in
	std::string strTransmit = "Transmit ";
	std::string strReceive = "Receive ";

	int transmit;
	int receive;

	for(int j=0; j < (int) nodesVector.at(0).getLayerCountTransmit().size(); j++){

		transmit = 0;
		receive = 0;

		for(int i=0; i < (int) nodesVector.size(); i++){

			transmit += nodesVector.at(i).getLayerCountTransmit().at(j);
			receive += nodesVector.at(i).getLayerCountReceive().at(j);

		}

		strTransmit += patch::to_string("Layer ");
		strTransmit += patch::to_string(j);
		strTransmit += patch::to_string(": ");
		strTransmit += patch::to_string(transmit);
		strTransmit += patch::to_string(" ");

		strReceive += patch::to_string("Layer ");
		strReceive += patch::to_string(j);
		strReceive += patch::to_string(": ");
		strReceive += patch::to_string(receive);
		strReceive += patch::to_string(" ");

		allTransmitperLayer.at(j) += transmit;
		allReceiveperLayer.at(j) += receive;

	}

	NS_LOG_UNCOND(strTransmit);
	NS_LOG_UNCOND(strReceive);

	// ---- Save number of average neighbours for this run
	int neighboursUntilTimeSlot;

	for(int n=1; n < numberOfTimeSlotsBeforeStop; n++){

		neighboursUntilTimeSlot = 0;

		for(int i=0; i < (int)nodesVector.size(); i++){
			for(int j=0; j < (int)nodesVector.at(i).getDiscoveredNodesList().size(); j++){

				// int tmp = n / printTimeslotStep;
				// int sweepEnd = tmp * printTimeslotStep;

				if(nodesVector.at(i).getDiscoveredNodesList().at(j).slot < n){
					neighboursUntilTimeSlot++;
				}
			}
		}

		// From all found neighbours to average neighbours per node
		double averageNeighoburs = (double)neighboursUntilTimeSlot / (double)nodes;
		averageNeighbours.push_back(averageNeighoburs);

	}

	// Distance to neighbour
	NS_LOG_UNCOND("Timeslot + Distance ");

	for(int i = 0; i < (int) nodesVector.size(); i++){
		for(int j = 0; j < (int) nodesVector.at(i).getDiscoveredNodesList().size(); j++){

			int id = nodesVector.at(i).getDiscoveredNodesList().at(j).nodeId;
			int timeslot = nodesVector.at(i).getDiscoveredNodesList().at(j).slot;
			NeighbourDiscovery nd;

			for(int n = 0; n < (int) nodesVector.size(); n++){
				if(nodesVector.at(n).getId() == id){
					nd = nodesVector.at(n);
				}
			}

			double distance = nodesVector.at(i).getWifiPhy()->GetMobility()->GetDistanceFrom(nd.getWifiPhy()->GetMobility());

			NS_LOG_UNCOND("Timeslot " << timeslot << " \t " << " Distance " << distance);

			beaconDistance.push_back(distance);
			beaconTimestamp.push_back(timeslot);
		}
	}
}

void AdaptiveNeighbourDiscovery::auswertungForAllRuns(){

	NS_LOG_FUNCTION("AdaptiveNeighbourDiscovery::auswertungForRun");

	// ---- Print average found neighbours

	NS_LOG_UNCOND("");

	NS_LOG_UNCOND("All runs #neighbours " << allRunsNeighbours << " #transmit " << allRunsTransmits <<
			" #receive " << allRunsReceives << " #beaconMessages " << allRunsBeaconMessages << " #interference " << allRunsInterference);

	NS_LOG_UNCOND("");

	std::string strTransmit = "Transmit ";
	std::string strReceive = "Receive ";

	for(int i=0; i < (int)allTransmitperLayer.size(); i++){
		strTransmit += patch::to_string("Layer ");
		strTransmit += patch::to_string(i);
		strTransmit += patch::to_string(": ");
		strTransmit += patch::to_string(allTransmitperLayer.at(i));
		strTransmit += patch::to_string(" ");

		strReceive += patch::to_string("Layer ");
		strReceive += patch::to_string(i);
		strReceive += patch::to_string(": ");
		strReceive += patch::to_string(allReceiveperLayer.at(i));
		strReceive += patch::to_string(" ");
	}

	NS_LOG_UNCOND(strTransmit);
	NS_LOG_UNCOND(strReceive);

	NS_LOG_UNCOND("");

	std::vector<double> elementsOfTimeSlot;
	std::string str;

	for(int i=0; i < (int)averageNeighbours.size()/runs; i++){

		str = "";
		str += patch::to_string(i+1);
		str += patch::to_string("\t");


		elementsOfTimeSlot.erase(elementsOfTimeSlot.begin(),elementsOfTimeSlot.end());

		for(int j=0; j < (int)(averageNeighbours.size()/(averageNeighbours.size()/runs)); j++){

			double tmp_averageNodes = averageNeighbours.at(j * (int)averageNeighbours.size()/runs + i);

			elementsOfTimeSlot.push_back(tmp_averageNodes);

		}

		std::sort(elementsOfTimeSlot.begin(), elementsOfTimeSlot.end());

		for(int i=0; i < (int) elementsOfTimeSlot.size(); i++){
			double tmp_value = elementsOfTimeSlot.at(i);

			str += patch::to_string(tmp_value);
			str += patch::to_string("\t");
		}

		NS_LOG_UNCOND(str);
	}

	NS_LOG_UNCOND("");

	int interval = 5;
	int maxDistance = 200;

	// Print distance to average timeslot
	for(int d=0; d < maxDistance; d=d+interval){

		int count = 0;

		double intervalAllTimeslots = 0;

		for(int i=0; i < (int)beaconDistance.size(); i++){
			if(beaconDistance.at(i) >= d && beaconDistance.at(i) < d+interval ){
				count++;
				intervalAllTimeslots += beaconTimestamp.at(i);
			}
		}

		double avgTimeslot = intervalAllTimeslots / count;

		NS_LOG_UNCOND("Distance " << d << " to " << d+interval << " = " << avgTimeslot);
	}

	NS_LOG_UNCOND("");

	// Print number of neighbours per distance

	int count = 0;

	for(int i=0; i < 200; i=i+5){

		count = 0;

		for(int j=0; j<(int) beaconDistance.size(); j++){
			if(beaconDistance.at(j) >= (double)i && beaconDistance.at(j) < (double)i+5){
				count++;
			}
		}

		NS_LOG_UNCOND("Number of nodes in distance " << i << " to " << i + 5 << " " << count);
	}
}

void AdaptiveNeighbourDiscovery::treeTest(){

	NS_LOG_FUNCTION("AdaptiveNeighbourDiscovery::treeTest");

	Tree m_Tree;

	std::vector<uint16_t> childrenPerNode;
	childrenPerNode.push_back(2);
	childrenPerNode.push_back(2);
	childrenPerNode.push_back(2);
	childrenPerNode.push_back(2);

	// Defines the beamwidth per layer
	std::vector<double> beamwidthPerLayer;
	beamwidthPerLayer.push_back(180.0);
	beamwidthPerLayer.push_back(90.0);
	beamwidthPerLayer.push_back(45.0);
	beamwidthPerLayer.push_back(22.5);

	// Defines the sector offset per layer
	std::vector<double> directionOffsetPerLayer;
	directionOffsetPerLayer.push_back(0.0);
	directionOffsetPerLayer.push_back(45.0);
	directionOffsetPerLayer.push_back(22.5); // sector offset
	directionOffsetPerLayer.push_back(11.25); // sector offset

	// Creates a transmit and receive probability tree
	m_Tree.createTree(childrenPerNode, beamwidthPerLayer, directionOffsetPerLayer, 0);

	// Set sectors with 180 degree beamwidth probability to 0
	m_Tree.resetProbabilityValue(m_Tree.getSector(1), 0.0);
	m_Tree.resetProbabilityValue(m_Tree.getSector(2), 0.0);

	// Fix
	for(int i = 7 ; i <=14; i++){
		m_Tree.resetProbabilityValue(m_Tree.getSector(i), 0.5);
	}

	// m_Tree.printTree();

}

void AdaptiveNeighbourDiscovery::resetValues(){

	NS_LOG_FUNCTION("AdaptiveNeighbourDiscovery::resetValues");

	nodesVector.erase(nodesVector.begin() , nodesVector.end());
}


void
AdaptiveNeighbourDiscovery::printDisoveredNeighours(){

	for(int i=0; i < nodes; i++){
		NS_LOG_UNCOND("Node " << nodesVector.at(i).getId() << " discovered ...");
		for(int j=0; j < (int)nodesVector.at(i).getDiscoveredNodesList().size(); j++){
			uint16_t id = nodesVector.at(i).getDiscoveredNodesList().at(j).nodeId;
			int ts = nodesVector.at(i).getDiscoveredNodesList().at(j).slot;
			double dd = nodesVector.at(i).getDiscoveredNodesList().at(j).discovered_direction;
			double db = nodesVector.at(i).getDiscoveredNodesList().at(j).discovered_beamwidth;
			double snr = nodesVector.at(i).getDiscoveredNodesList().at(j).discoverd_snr;

			NS_LOG_UNCOND("  Node " << id << " in timeslot " << ts << " direction " << dd << " beamwidth " << db << " snr " << snr);
		}
	}
}

static void PrintAdaptiveND (int argc, char *argv[])
{
  AdaptiveNeighbourDiscovery experiment;
  struct AdaptiveNeighbourDiscovery::Input input;

  CommandLine cmd;
  cmd.AddValue ("PacketSize", "The size of each packet sent", input.packetSize);
  cmd.AddValue ("TxMode", "The mode to use to send each packet", input.txMode);
  cmd.AddValue ("TxPowerLevel", "The power level index to use to send each packet", input.txPowerLevel);
  // cmd.Parse (argc, argv);

  experiment.Initialze();

}

//
// Choose neighbour discovery modus
//
// List options: ./waf --run scratch/neighbor-discovery
// Choosen option: ./waf --run "scratch/neighbor-discovery OptionName"

int main (int argc, char *argv[])
{

  PrintAdaptiveND (argc, argv);

  return 0;
}
