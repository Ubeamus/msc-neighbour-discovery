/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2006,2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Mathieu Lacage, <mathieu.lacage@sophia.inria.fr>
 */

#include "ns3/packet.h"
#include "ns3/simulator.h"
#include "ns3/mobility-model.h"
#include "ns3/net-device.h"
#include "ns3/node.h"
#include "ns3/log.h"
#include "ns3/pointer.h"
#include "ns3/object-factory.h"
#include "yans-wifi-channel.h"
#include "ns3/propagation-loss-model.h"
#include "ns3/propagation-delay-model.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("YansWifiChannel");

NS_OBJECT_ENSURE_REGISTERED (YansWifiChannel);

TypeId
YansWifiChannel::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::YansWifiChannel")
    .SetParent<WifiChannel> ()
    .SetGroupName ("Wifi")
    .AddConstructor<YansWifiChannel> ()
    .AddAttribute ("PropagationLossModel", "A pointer to the propagation loss model attached to this channel.",
                   PointerValue (),
                   MakePointerAccessor (&YansWifiChannel::m_loss),
                   MakePointerChecker<PropagationLossModel> ())
    .AddAttribute ("PropagationDelayModel", "A pointer to the propagation delay model attached to this channel.",
                   PointerValue (),
                   MakePointerAccessor (&YansWifiChannel::m_delay),
                   MakePointerChecker<PropagationDelayModel> ())
  ;
  return tid;
}

YansWifiChannel::YansWifiChannel ()
{
  directionalMode = true;
}

YansWifiChannel::~YansWifiChannel ()
{
  NS_LOG_FUNCTION_NOARGS ();
  m_phyList.clear ();
}

void
YansWifiChannel::SetPropagationLossModel (Ptr<PropagationLossModel> loss)
{
  m_loss = loss;
}

void
YansWifiChannel::SetPropagationDelayModel (Ptr<PropagationDelayModel> delay)
{
  m_delay = delay;
}

// Helper Methods
static std::string doubleToString(double pValue)
{ 	std::ostringstream strs;
	strs << pValue;
	return strs.str(); }

/*
static std::string intToString(int pValue){
	std::ostringstream strs;
	strs << pValue;
	return strs.str();
}
*/


const double calculateRadian(double x1, double y1, double x2, double y2)
{ return std::atan((y1 - y2) / (x1 - x2)); }

const double toDegree(const double radian)
{ return radian * 180.0 / 3.14159265; }

const double toRadian(const double degree)
{ return degree * 3.14159265 / 180.0; }

void
YansWifiChannel::Send (Ptr<YansWifiPhy> sender, Ptr<const Packet> packet, double txPowerDbm,
                       WifiTxVector txVector, WifiPreamble preamble, enum mpduType mpdutype, Time duration, CosineAntennaModel *antennaModel) const
{
  NS_LOG_FUNCTION("LOG INFO: Wifi Channel: Send");

  // NS_LOG_UNCOND("Channel " << txPowerDbm);

  Ptr<MobilityModel> senderMobility = sender->GetMobility ()->GetObject<MobilityModel> ();
  NS_ASSERT (senderMobility != 0);
  uint32_t j = 0;
  for (PhyList::const_iterator i = m_phyList.begin (); i != m_phyList.end (); i++, j++)
    {
	  // NS_LOG_DEBUG("LOG INFO: Wifi Channel: Number of nodes " << m_phyList.size());
      if (sender != (*i))
        {
    	  // ("LOG INFO: Wifi Channel: Send");
          //For now don't account for inter channel interference
          if ((*i)->GetChannelNumber () != sender->GetChannelNumber ())
            {
              continue;
            }

          if(directionalMode){

          // Bug 1: txPowerDbm has kind of arbitrary values, therefore set new value here
          txPowerDbm = 10 + sender->GetTxGain();
          // NS_LOG_UNCOND("txPowerDBm " << txPowerDbm);

          NS_LOG_DEBUG("directional start");

          // Formula Values
          NS_LOG_INFO("Node " << sender->getId() << " GTX " << sender->GetTxGain());

          Ptr<MobilityModel> receiverMobility = (*i)->GetMobility ()->GetObject<MobilityModel> ();
          Time delay = m_delay->GetDelay (senderMobility, receiverMobility);

          // Formula Values - Just print out of FSPL
          double FSPL = m_loss->CalcRxPower (0.0, senderMobility, receiverMobility);
          NS_LOG_INFO("Node " << sender->getId() << " FSPL " << FSPL);

          // --- Adjust txPowerDbm to current receiver ---

          // Position from sender and receiver
          Vector senderPosition = senderMobility->GetPosition();
          Vector receiverPosition = receiverMobility->GetPosition();

          // Direction from sender to receiver
          Vector directionSenderReceiver;
          directionSenderReceiver.x = receiverPosition.x - senderPosition.x;
          directionSenderReceiver.y = receiverPosition.y - senderPosition.y;
          directionSenderReceiver.z = receiverPosition.z - senderPosition.z;

          double x = directionSenderReceiver.x;
          double y = directionSenderReceiver.y;

          // Direction from receiver to sender
          double angleInRadians = std::atan2(y, x);

          NS_LOG_DEBUG("LOG DEBUG: angleInRadians " << doubleToString(angleInRadians));
          double directionDegreeSenderReceiver = (angleInRadians / M_PI) * 180.0;

          // Angle from sender to receiver and antenna orientation
          double orientationDegreeSender = antennaModel->GetOrientation();
          NS_LOG_DEBUG("LOG DEBUG: direction sender to receiver " << doubleToString(directionDegreeSenderReceiver));
          NS_LOG_DEBUG("LOG DEBUG: antenna orientation sender " << doubleToString(orientationDegreeSender));

          // Difference from direction and orientation degree from sender node
          double degreeDifferenz;

          if(directionDegreeSenderReceiver > orientationDegreeSender){
			  degreeDifferenz = directionDegreeSenderReceiver - orientationDegreeSender;
          } else {
        	  degreeDifferenz = orientationDegreeSender - directionDegreeSenderReceiver;
          }

          // NS_LOG_DEBUG("LOG DEBUG: degree difference before " << doubleToString(degreeDifferenz));
          if(degreeDifferenz > 180) {degreeDifferenz = 360-degreeDifferenz;}

          NS_LOG_DEBUG("LOG DEBUG: degree difference " << doubleToString(degreeDifferenz));

          // Calculate txPowerDbm gain loss
          Angles *anglesDirectionSenderReceiver = new Angles(directionSenderReceiver);
          double gainLoss = antennaModel->GetGainDb(*anglesDirectionSenderReceiver);

          delete anglesDirectionSenderReceiver;

          // Formula Values
          NS_LOG_INFO("Node " << sender->getId() << " LATX " << gainLoss);

          // Old values
          NS_LOG_DEBUG("LOG DEBUG: txPowerDbm " << doubleToString(txPowerDbm));

          NS_LOG_DEBUG("sender - txPowerDbm " << txPowerDbm << " gainLoss " << gainLoss);
          txPowerDbm = txPowerDbm + gainLoss; // gainLoss is negative

          // New values
          NS_LOG_DEBUG("LOG DEBUG: gainLoss " << doubleToString(gainLoss));
          NS_LOG_DEBUG("LOG DEBUG: adjusted txPowerDbm - gainLoss " << doubleToString(txPowerDbm));

          // --- End adjust txPowerDbm to current receiver ---
          double rxPowerDbm = m_loss->CalcRxPower (txPowerDbm, senderMobility, receiverMobility);

          // Oxygen absorbtion calculation (15 dB/km)
          double distance = senderMobility->GetDistanceFrom (receiverMobility);
          double oxygenAbsorbtion = (distance / 1000) * 15 * (-1);
          rxPowerDbm = rxPowerDbm + oxygenAbsorbtion;

          // --- Adjust rxPowerDbm to current receiver ---

          NS_LOG_DEBUG("directional start receiver");
          Vector directionReceiverSender; // direction from sender to receiver
          directionReceiverSender.x = senderPosition.x - receiverPosition.x;
          directionReceiverSender.y = senderPosition.y - receiverPosition.y;
          directionReceiverSender.z = senderPosition.z - receiverPosition.z;

          x = directionReceiverSender.x;
          y = directionReceiverSender.y;

          // Direction from receiver to sender
          angleInRadians = std::atan2(y, x);
          double directionDegreeReceiverSender = (angleInRadians / M_PI) * 180.0;

          // Direction of the antenna
          double orientationDegreeReceiver = (*i)->getCosineAntennaModel()->GetOrientation();

          NS_LOG_DEBUG("LOG DEBUG: antenna orientation receiver " << doubleToString(orientationDegreeReceiver));
          NS_LOG_DEBUG("LOG DEBUG: direction receiver to sender " << doubleToString(directionDegreeReceiverSender));

          // Difference from direction and orientation degree from sender
          double degreeDifferenzReceiver;

          if(directionDegreeReceiverSender > orientationDegreeReceiver){
        	  degreeDifferenzReceiver = directionDegreeReceiverSender - orientationDegreeReceiver;
        	  NS_LOG_DEBUG("degreeDifferenzReceiver " << degreeDifferenzReceiver);
          } else {
        	  degreeDifferenzReceiver = orientationDegreeReceiver - directionDegreeReceiverSender;
        	  NS_LOG_DEBUG("degreeDifferenzReceiver " << degreeDifferenzReceiver);
          }

          if(degreeDifferenzReceiver > 180) {angleInRadians = 360-degreeDifferenzReceiver;}

          NS_LOG_DEBUG("LOG DEBUG: degree difference " << doubleToString(degreeDifferenzReceiver));

          // Calculate rx gain loss
          Angles *anglesDirectionReceiverSender= new Angles(directionReceiverSender);
          double gainLossReceiver = (*i)->getCosineAntennaModel()->GetGainDb(*anglesDirectionReceiverSender);

          delete anglesDirectionReceiverSender;

          // Formula Values
          NS_LOG_INFO("Node " << sender->getId() << " LARX " << gainLossReceiver);
          NS_LOG_INFO("Node " << sender->getId() << " oxygen absorbtion " << oxygenAbsorbtion);

          // Old values
          NS_LOG_DEBUG("LOG DEBUG: receive rxPowerDbm " << doubleToString(rxPowerDbm));

          NS_LOG_DEBUG("receiver - txPowerDbm " << rxPowerDbm << " gainLoss " << gainLossReceiver);
          rxPowerDbm = rxPowerDbm + gainLossReceiver; // gainLossReceiver is negative

          // New values
          NS_LOG_DEBUG("Log DEBUG: receive gainLoss " << doubleToString(gainLossReceiver));
          NS_LOG_DEBUG("Log DEBUG: receive adjusted txPowerDbm - gainLoss " << doubleToString(rxPowerDbm));

          // --- End adjust rxPowerDbm to current receiver ---
          Ptr<Packet> copy = packet->Copy ();
          Ptr<Object> dstNetDevice = m_phyList[j]->GetDevice ();
          uint32_t dstNode;

          if (dstNetDevice == 0)
            {
              dstNode = 0xffffffff;
            }
          else
            {
              dstNode = dstNetDevice->GetObject<NetDevice> ()->GetNode ()->GetId ();
            }

          struct Parameters parameters;
          parameters.rxPowerDbm = rxPowerDbm;
          parameters.type = mpdutype;
          parameters.duration = duration;
          parameters.txVector = txVector;
          parameters.preamble = preamble;

          Simulator::ScheduleWithContext (dstNode,
                                          delay, &YansWifiChannel::Receive, this,
                                          j, copy, parameters);

          } else {
        	Ptr<MobilityModel> receiverMobility = (*i)->GetMobility ()->GetObject<MobilityModel> ();
			Time delay = m_delay->GetDelay (senderMobility, receiverMobility);
			double rxPowerDbm = m_loss->CalcRxPower (txPowerDbm, senderMobility, receiverMobility);
			NS_LOG_DEBUG ("propagation: txPower=" << txPowerDbm << "dbm, rxPower=" << rxPowerDbm << "dbm, " <<
						  "distance=" << senderMobility->GetDistanceFrom (receiverMobility) << "m, delay=" << delay);
			Ptr<Packet> copy = packet->Copy ();
			Ptr<Object> dstNetDevice = m_phyList[j]->GetDevice ();
			uint32_t dstNode;
			if (dstNetDevice == 0)
			  {
				dstNode = 0xffffffff;
			  }
			else
			  {
				dstNode = dstNetDevice->GetObject<NetDevice> ()->GetNode ()->GetId ();
			  }

			struct Parameters parameters;
			parameters.rxPowerDbm = rxPowerDbm;
			parameters.type = mpdutype;
			parameters.duration = duration;
			parameters.txVector = txVector;
			parameters.preamble = preamble;

			Simulator::ScheduleWithContext (dstNode,
											delay, &YansWifiChannel::Receive, this,
											j, copy, parameters);
          }
        }
    }
}

void
YansWifiChannel::Receive (uint32_t i, Ptr<Packet> packet, struct Parameters parameters) const
{
  NS_LOG_FUNCTION("Wifi Channel: Receive");
  m_phyList[i]->StartReceivePreambleAndHeader (packet, parameters.rxPowerDbm, parameters.txVector, parameters.preamble, parameters.type, parameters.duration);
}

uint32_t
YansWifiChannel::GetNDevices (void) const
{
  return m_phyList.size ();
}

Ptr<NetDevice>
YansWifiChannel::GetDevice (uint32_t i) const
{
  return m_phyList[i]->GetDevice ()->GetObject<NetDevice> ();
}

void
YansWifiChannel::Add (Ptr<YansWifiPhy> phy)
{
  m_phyList.push_back (phy);
}

int64_t
YansWifiChannel::AssignStreams (int64_t stream)
{
  int64_t currentStream = stream;
  currentStream += m_loss->AssignStreams (stream);
  return (currentStream - stream);
}

} //namespace ns3
