/*
 * SectorNew.h
 *
 *  Created on: Jan 4, 2016
 *      Author: johannes
 */

#ifndef SECTORNEW_H_
#define SECTORNEW_H_

#include <vector>
#include <stdint.h>

namespace ns3 {

class SectorNew {
public:
	SectorNew();
	virtual ~SectorNew();

	uint16_t id;

	SectorNew *parent;

	// Node
	double beamwidth;
	double direction;

	// Children
	std::vector<SectorNew*> children; // child 1, child 2, ... , child n
	std::vector<double> probabilities; // node, p(child 1), p(child 2), ... , p(child n)
};

} /* namespace ns3 */

#endif /* SECTORNEW_H_ */
