/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
#ifndef NEIGHBOUR_DISCOVERY_H
#define NEIGHBOUR_DISCOVERY_H

#include "ns3/wifi-net-device.h"
#include "ns3/yans-wifi-channel.h"
#include "ns3/yans-wifi-phy.h"
#include "ns3/propagation-loss-model.h"
#include "ns3/propagation-delay-model.h"
#include "ns3/error-rate-model.h"
#include "ns3/yans-error-rate-model.h"
#include "ns3/ptr.h"
#include "ns3/mobility-model.h"
#include "ns3/constant-position-mobility-model.h"
#include "ns3/vector.h"
#include "ns3/packet.h"
#include "ns3/simulator.h"
#include "ns3/nstime.h"
#include "ns3/command-line.h"
#include "ns3/flow-id-tag.h"
#include "ns3/wifi-tx-vector.h"
#include "ns3/cosine-antenna-model.h"
#include "ns3/NDPacket.h"
#include "ns3/flow-id-tag.h"
#include "Tree.h"


namespace ns3 {

class NeighbourDiscovery
{
public:
  struct Input
  {
    Input ();
    std::string txMode;
    uint8_t txPowerLevel;
    uint32_t packetSize;
  };
  struct Output
  {
    uint32_t received;
  };

  struct DiscoverdNodes
  {
	  uint32_t nodeId;
	  double discovered_direction;
	  double discovered_beamwidth;
	  double discoverd_snr;
	  int slot;
  };

  NeighbourDiscovery ();
  ~NeighbourDiscovery ();
  Ptr<YansWifiPhy> CreateNode(Ptr<YansWifiChannel> channel, Ptr<ErrorRateModel> error, std::vector<NeighbourDiscovery>* m_nodesVector);

  // Helper functions
  std::string intToString(int pValue);
  std::string doubleToString(double pValue);

  const double toDegree(const double radian);
  const double toRadian(const double degree);

  double adjustGain(double pBeamwidth);

  // Setter
  void setId(int xPos);

  void setXPos(double xPos);
  void setYPos(double yPos);
  void setBeamwidth(double beamwidth);
  void setDirection(double direction);
  void setDirectionSector(double sector);
  void setAntennaOffset(double offset);

  void setTimeSlot(int slot);
  void setSend(bool send);

  void setHandshake(bool handshake);

  void setTxGain(double txGain);
  void setRxGain(double rxGain);

  void setMaxTimeSlot(int p_maxTimeSlot);

  void setDiscoveredNodes (DiscoverdNodes node);

  // Getter
  int getId();

  double getXPos();
  double getYPos();
  double getBeamwidth();
  double getDirection();
  double getAntennaOffset();

  int getTimeSlot(); // Slot
  bool getSweep(); // Sweep

  double getTxGain(); // txGain
  double getRxGain(); // rxGain

  // Evaluation count variables
  int getTransmitCount();
  int getReceiveCount();

  int getInterferenceCount();
  int getBeaconMessagesCount();

  std::vector<int> getLayerCountTransmit();
  std::vector<int> getLayerCountReceive();

  Tree getTransmitTree();
  Tree getReceiveTree();

  int getAntennaSectors();

  // disoverd neighbours list and print function
  std::vector<DiscoverdNodes> getDiscoveredNodesList();
  void printDisoveredNeighours();

  void registerBeaconMessage(uint16_t p_senderNodeId, uint16_t p_senderSectorId, uint16_t p_replySlot , double p_direction, double p_beamwidth, int ack);

  // node
  Ptr<YansWifiPhy> getWifiPhy();

  unsigned int randRange(unsigned int min, unsigned int max);

  // Send / Receive Methods
  void Send ();
  void SendWithInfo (int sectorId, int replySlot);
  void Receive (Ptr<Packet> p, double snr, WifiTxVector txVector, enum WifiPreamble preamble);

  // Log function
  void logNodeInfo();

  // Continuing transmit / receive
  void initializeContinuingTransmit(int continuingTimeSlotLength);
  void initializeContinuingReceive(int continuingTimeSlotLength);

  // Sweep ND - all nodes do complete sweep
  void initializeSweep(double directionSteering);
  void sweepReceive(int timeSlot, double directionSteering);
  void sweepTransmit(int timeSlot, double directionSteering);

  // Adaptive - ND
  void initializeAdaptive();
  void topAdaptive();
  void fillBeaconInterval();
  void scheduleBeaconInterval();
  void addToPriorityList(int sectorId, int replySlot);
  void adaptiveTransmit(int p_sectorIndex, int p_sectorTimeSlot);
  void adaptiveReceive(int p_sectorIndex, int p_sectorTimeSlot);
  void adaptiveCheckPriority();

  void sortPriorityList();
  void bringTransmitSectorsInFront();

  void evaluateLayerCountTransmit(double beamwidth);
  void evaluateLayerCountReceive(double beamwidth);

  // Create tree
  void createTree();

  // double rand
  double fRand(double fMin, double fMax);

  // schedule transmit/receive
  void scheduleBeaconAt(int scheduleSlot);
  void scheduleReceiveAt(int scheduleSlot);

  // random sector / directin
  double randomSector();
  double randomDirection();

  // convert sector to direction and other way around
  double sectorToDirection(double sector);
  double directionToSector(double direction);

  bool randomMode();
  bool randomModeAdaptive();

  int returnRandomAvailableBeaconInterval();

  void setBeamwidthDirectionTimeslot(double beamwidth, double direction, int timeslot);

  // collision
  void registerCollision(Ptr<Packet> packet);

private:

  Ptr<YansWifiPhy> m_node; // Phy

  // ID
  uint16_t m_id;

  // Mode
  bool m_modeSend;
  bool m_fixPriorityMode;

  // Beacon Interval Length
  double m_beaconIntervalLength;
  int beaconIntervalReplySkip;

  // Send Receive Probability
  double m_sendReceiveProbability;

  // Use priority list
  bool m_priority;

  // Time Slot
  int m_timeSlot;

  // Antenna Sectors
  int m_antennaDirections;
  int m_antennaOffset;

  // Tree
  Tree m_Tree_Transmit;
  Tree m_Tree_Receive;

  // -- Evaluation variables

  // Count variables
  int m_transmitCount;
  int m_receiveCount;

  int m_interferenceCount;
  int m_beaconCount;

  // Tree layer count
  std::vector<int> layerCountTransmit;
  std::vector<int> layerCountReceive;

  // -- End Evaluation variables

  // Setup variables
  bool setup;
  bool m_handshake;

  int freePriorityListIndex;

  // Sector
  struct sector {
	  int sectorId;
	  int timeSlot;
	  bool transmit;
	  bool ack;
	  double direction;
	  double beamwidth;
  } ;

  // Sectors
  std::vector<sector> m_sectors;

  // Priority List
  std::vector<sector> m_priorityList;
  sector s_priorityList[];

  // Break condition
  int m_maxTimeSlot;

  // List of discovered neighbours
  std::vector< DiscoverdNodes > m_discovered_list;



  struct Input m_input;
  struct Output m_output;

  bool firstRun;

};

}

#endif /* NEIGHBOUR_DISCOVERY_H */

