/*
 * tree.h
 *
 *  Created on: Dec 16, 2015
 *      Author: johannes
 */

#include "SectorInformation.h"
#include "Sector.h"
#include "SectorNew.h"
#include <stdint.h>
#include <vector>


#ifndef TREE_H_
#define TREE_H_

namespace ns3 {

class Tree {
public:
	Tree();
	virtual ~Tree();

	void createTree(std::vector<uint16_t> childrenPerNode,  // 4, 2
					std::vector<double> beamwidthPerLayer,  // 90,60-20
					std::vector<double> offset,				// 0, 10 (+10,-10,+20,-20)
					int offsetDirection);

	void initializeProbabilityValues();
	void printTree();

	SectorNew* getMostProbableSector();
	SectorNew* getSector(int sectorId);

	// Update Probabilities

	// transmit
	void updateTransmitProbability(Sector *sector);

	void resetProbabilityValue(SectorNew *sector, double value);

	// Helper
	double fRand(double fMin, double fMax);

	void updateSector(SectorNew *sector, double value);
	void updateParent(SectorNew *sector, double value);

	void recursiveUpdateSector(SectorNew *sector, double value);
	void recursiveUpdateParent(SectorNew *sector, double value);

	std::vector<double> normalize(std::vector<double> valuesToNormalize);
	void proportionalUpdateParent(int indexPos, double value, SectorNew *parent);
	void proportionalUpdateSector(double value, SectorNew *sector);

private:

	// Root Node
	SectorNew rootNode;

	// Antenna values
	int antennaDirections;

	// Probabilities
	double sharedChildrenProbabilityValue;

	// Probability values
	double minSectorProbabilityValue;
	double minChildProbabilityValue;

	double maxSectorProbabilityValue;
	double maxChildProbabilityValue;

};

} /* namespace ns3 */

#endif /* TREE_H_ */
