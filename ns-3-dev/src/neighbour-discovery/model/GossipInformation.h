/*
 * GossipInformation.h
 *
 *  Created on: Dec 16, 2015
 *      Author: johannes
 */

#ifndef GOSSIPINFORMATION_H_
#define GOSSIPINFORMATION_H_

struct GossipInformation
{
	uint16_t id;
	double direction;
	double beamwidth;
	double signalStrength;
};

#endif /* GOSSIPINFORMATION_H_ */
