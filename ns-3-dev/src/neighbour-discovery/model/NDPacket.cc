/*
 * NDPacket.cc
 *
 *  Created on: Nov 3, 2015
 *      Author: johannes
 */

#include "NDPacket.h"

namespace ns3 {

NDPacket::NDPacket ()
{
  // we must provide a public default constructor,
  // implicit or explicit, but never private.
}
NDPacket::~NDPacket ()
{
}

TypeId
NDPacket::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::MyHeader")
    .SetParent<Header> ()
    .AddConstructor<NDPacket> ()
  ;
  return tid;
}
TypeId
NDPacket::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

void
NDPacket::Print (std::ostream &os) const
{
  // This method is invoked by the packet printing
  // routines to print the content of my header.
  //os << "data=" << m_data << std::endl;
  os << "data id=" << m_id << " sectorId " << m_sectorId << " replySlot " << m_replySlot << " direction " << m_direction << " beamwidth " << m_beamwidth << " ack " << m_ack;
}
uint32_t
NDPacket::GetSerializedSize (void) const
{
  // we reserve 12 bytes for our header.
  return 12;
}
void
NDPacket::Serialize (Buffer::Iterator start) const
{
  // we can serialize two bytes at the start of the buffer.
  // we write them in network byte order.
  start.WriteHtonU16 (m_id);
  start.WriteHtonU16(m_ack);
  start.WriteHtonU16(m_sectorId);
  start.WriteHtonU16(m_replySlot);
  start.WriteHtonU16(m_direction);
  start.WriteHtonU16(m_beamwidth);
}
uint32_t
NDPacket::Deserialize (Buffer::Iterator start)
{
  // we can deserialize two bytes from the start of the buffer.
  // we read them in network byte order and store them
  // in host byte order.
  m_id = start.ReadNtohU16();
  m_ack = start.ReadNtohU16();
  m_sectorId = start.ReadNtohU16();
  m_replySlot = start.ReadNtohU16();
  m_direction = start.ReadNtohU16();
  m_beamwidth = start.ReadNtohU16();

  // we return the number of bytes effectively read.
  return 12;
}

void NDPacket::SetId (uint16_t p_Id){
	m_id = p_Id;
}
uint16_t NDPacket::GetId (void) const{
	return m_id;
}

void NDPacket::SetSectorInformation (uint16_t pSectorId){
	m_sectorId = pSectorId;
}

uint16_t NDPacket::GetSectorInformation (void) const{
	return m_sectorId;
}

void NDPacket::SetReplySlots (uint16_t p_replySlot){
	m_replySlot = p_replySlot;
}
uint16_t NDPacket::GetReplySlots (void) const{
	return m_replySlot;
}

void NDPacket::SetBeamwidth (uint16_t beamwidth){
	m_beamwidth = beamwidth;
}

uint16_t NDPacket::GetBeamwidth (void) const{
	return m_beamwidth;
}

void NDPacket::SetDirection (uint16_t direction){
	m_direction = direction;
}

uint16_t NDPacket::GetDirection(void) const{
	return m_direction;
}

void NDPacket::SetAck (int p_ack){
	m_ack = p_ack;
}
int NDPacket::GetAck (void) const{
	return m_ack;
}

void NDPacket::SetGossipInformation (std::vector<GossipInformation> p_gossipInformation){
	m_gossip = p_gossipInformation;
}
std::vector<GossipInformation> NDPacket::GetGossipInformation (void) const{
	return m_gossip;
}

} /* namespace ns3 */
