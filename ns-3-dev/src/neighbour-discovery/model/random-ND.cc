/*
 * randomND.cc
 *
 *  Created on: Oct 28, 2015
 *      Author: johannes
 */

#include "random-ND.h"

#include <stdio.h>
#include <stdlib.h>

namespace ns3 {

RandomND::RandomND() {
	// TODO Auto-generated constructor stub
	numNodes = 0;
	areaLength = 0;
	areaWidth = 0;

}

RandomND::~RandomND() {
	// TODO Auto-generated destructor stub
}

// Getter
int const& RandomND::getNumNodes() const {
	return areaLength; }

int const& RandomND::getAreaLength() const {
	return areaLength; }

int const& RandomND::getAreaWidth() const {
	return areaLength; }

// Setter
void RandomND::setNumNodes(int const& newNumNodes) {
	numNodes = newNumNodes; }

void RandomND::setAreaLength(int const& newNumNodes) {
	areaLength = newNumNodes; }

void RandomND::setAreaWidth(int const& newNumNodes) {
	areaWidth = newNumNodes; }

Vector RandomND::randomPlaceNodes(){
	Vector randomPositionVector;
	randomPositionVector.x = (rand() % (getAreaLength() + 1 ));
	randomPositionVector.y = (rand() % (getAreaWidth() + 1 ));
	randomPositionVector.z = 0.0;

	return randomPositionVector;
}

// Returns a value between 0 and 359 degree
int RandomND::setNewOrientation(){
	return (rand() % (360));
}

void RandomND::sendBeacon(){
	// send beacon in orientation
}

void receiveBeacon(){
	// receive beacon in orientation
}



} /* namespace ns3 */
