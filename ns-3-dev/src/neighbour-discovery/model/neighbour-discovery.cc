/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */

#include "neighbour-discovery.h"
#include "ns3/log.h"
#include <math.h>
#include <stdio.h>      /* printf, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <algorithm>
#include "ns3/nstime.h"
#include "ns3/random-variable-stream.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("NeighbourDiscovery");

NeighbourDiscovery::NeighbourDiscovery (){

	// Initialize
	m_modeSend = false;
	m_fixPriorityMode = false;
	m_priority = false;

	m_sendReceiveProbability = 0.5;
	m_antennaDirections = 360;
	m_timeSlot = 1;

	// Used to initialize parameters in first run
	firstRun = true;

	// Evaluation count variables
	m_transmitCount = 0;
	m_receiveCount = 0;

	m_beaconCount = 0;
	m_interferenceCount = 0;

	// Set value for break condition
	m_maxTimeSlot = -1;

	// s_priorityList

	sector s_priorityList[12];

	sector dummySector = {};
	dummySector.timeSlot = -1;

	for(int i = 1; i < 12; i++){
		s_priorityList[0] = dummySector;
	}


}

NeighbourDiscovery::~NeighbourDiscovery (){}

NeighbourDiscovery::Input::Input ()
  : txMode ("OfdmRate6Mbps"),
    txPowerLevel (0),
    packetSize (2304) {}

std::string NeighbourDiscovery::intToString(int pValue){
	std::ostringstream strs;
	strs << pValue;
	return strs.str();}

std::string NeighbourDiscovery::doubleToString(double pValue){
	std::ostringstream strs;
	strs << pValue;
	return strs.str();}

const double NeighbourDiscovery::toDegree(const double radian){
	return radian * 180.0 / 3.14159265; }

const double NeighbourDiscovery::toRadian(const double degree){
	return degree * 3.14159265 / 180.0; }

double
NeighbourDiscovery::adjustGain(double hpbw_InRadian){
	return 10*log10(pow((1.6162/sin(hpbw_InRadian/2)),2));
	// return 10*log10((1.6162/sin(hpbw_InRadian/2)));
}
// --- Getter ---
double NeighbourDiscovery::getXPos() {
	return m_node->GetMobility()->GetPosition().x;
}
double NeighbourDiscovery::getYPos() {
	return m_node->GetMobility()->GetPosition().y;
}
int NeighbourDiscovery::getTimeSlot(){
	return m_timeSlot;
}
double NeighbourDiscovery::getBeamwidth() {
	return m_node->getCosineAntennaModel()->GetBeamwidth();
}
double NeighbourDiscovery::getDirection() {
	return m_node->getCosineAntennaModel()->GetOrientation();
}
int NeighbourDiscovery::getId() {
	return m_id;
}
bool NeighbourDiscovery::getSweep(){
	return m_modeSend;
}
int NeighbourDiscovery::getAntennaSectors(){
	return m_antennaDirections;
}
std::vector<NeighbourDiscovery::DiscoverdNodes > NeighbourDiscovery::getDiscoveredNodesList(){
	return m_discovered_list;
}
Ptr<YansWifiPhy> NeighbourDiscovery::getWifiPhy(){
	return m_node;
}
double NeighbourDiscovery::getAntennaOffset(){
	return m_antennaOffset;
}
int NeighbourDiscovery::getInterferenceCount(){
	return m_interferenceCount;
}
int NeighbourDiscovery::getBeaconMessagesCount(){
	return m_beaconCount;
}
int NeighbourDiscovery::getTransmitCount(){
	return m_transmitCount;
}
int NeighbourDiscovery::getReceiveCount(){
	return m_receiveCount;
}
std::vector<int> NeighbourDiscovery::getLayerCountTransmit(){
	return layerCountTransmit;
}
std::vector<int> NeighbourDiscovery::getLayerCountReceive(){
	return layerCountReceive;
}
Tree NeighbourDiscovery::getTransmitTree(){
	return m_Tree_Transmit;
}
Tree NeighbourDiscovery::getReceiveTree(){
	return m_Tree_Receive;
}

// --- Setter ---
void NeighbourDiscovery::setXPos(double xPos){
	m_node->GetMobility()->SetPosition(Vector (xPos, getYPos(), 0.0));
}
void NeighbourDiscovery::setYPos(double yPos){
	m_node->GetMobility()->SetPosition(Vector (getXPos(), yPos, 0.0));
}
void NeighbourDiscovery::setTimeSlot(int slot){
	m_timeSlot = slot;
}
void NeighbourDiscovery::setBeamwidth(double beamwidth){
	m_node->getCosineAntennaModel()->SetBeamwidth(beamwidth);
	setTxGain(getBeamwidth());
	setRxGain(getBeamwidth());
}
void NeighbourDiscovery::setDirection(double direction){
	double mod_direction = fmod(direction, 360); // bug? does not work
	m_node->getCosineAntennaModel()->SetOrientation(mod_direction);
}
void NeighbourDiscovery::setDirectionSector(double sector){
	double direction = sector * (360/ getAntennaSectors());
	setDirection(direction);
}
void NeighbourDiscovery::setAntennaOffset(double offset){
	m_antennaOffset = offset;
}

void NeighbourDiscovery::setId(int id){
	m_id = id;
	m_node->setId(id);
}
void NeighbourDiscovery::setSend(bool send){
	m_modeSend = send;
}
void NeighbourDiscovery::setHandshake(bool handshake){
	m_handshake = handshake;
}

void NeighbourDiscovery::setTxGain(double hpbw){
	m_node->SetTxGain(adjustGain(toRadian(hpbw)));
}

void NeighbourDiscovery::setRxGain(double hpbw){
	m_node->SetRxGain(adjustGain(toRadian(hpbw)));
}

void NeighbourDiscovery::setMaxTimeSlot(int p_maxTimeSlot){
 	m_maxTimeSlot = p_maxTimeSlot;
}

void NeighbourDiscovery::setDiscoveredNodes (DiscoverdNodes node){
	m_discovered_list.push_back(node);
}

unsigned int
NeighbourDiscovery::randRange(unsigned int min, unsigned int max){
double scaled = (double) rand() / RAND_MAX;
return (max - min +1)*scaled + min;
}

void
NeighbourDiscovery::Send (){

  // NS_LOG_UNCOND("Transmit - Node " << getId() << " direction " << getDirection() << " beamwidth " << getBeamwidth());

  // Update variables
  m_transmitCount++;

  Ptr<Packet> p = Create<Packet> (m_input.packetSize);

  // Add Send Node ID
  NDPacket ndPacket;
  ndPacket.SetId(getId());
  ndPacket.SetSectorInformation(-1);
  ndPacket.SetReplySlots(-1);
  p->AddHeader(ndPacket);

  WifiMode mode = WifiMode (m_input.txMode);
  WifiTxVector txVector;
  txVector.SetTxPowerLevel (m_input.txPowerLevel);
  txVector.SetMode (mode);
  mpduType mpdu = NORMAL_MPDU;

  NS_LOG_INFO("SEND ---- (" << Simulator::Now() << ")");

  m_node->SendPacket (p, txVector, WIFI_PREAMBLE_SHORT, mpdu);
}

void
NeighbourDiscovery::SendWithInfo (int pMSectorIndex, int replySlot){

  // Update variables
  m_transmitCount++;

  Ptr<Packet> p = Create<Packet> (m_input.packetSize);

  // Add Send Node ID, Sector Information and Reply Slot
  NDPacket ndPacket;
  ndPacket.SetId(getId());

  // Set ack flag
  if(m_sectors.at(pMSectorIndex).ack == 1){
	  ndPacket.SetAck(1);
  } else {
	  ndPacket.SetAck(0);
  }
  ndPacket.SetSectorInformation(m_sectors.at(pMSectorIndex).sectorId);
  ndPacket.SetReplySlots(replySlot);
  ndPacket.SetDirection((int) getDirection());
  ndPacket.SetBeamwidth((int) getBeamwidth());
  p->AddHeader(ndPacket);


  WifiMode mode = WifiMode (m_input.txMode);
  WifiTxVector txVector;
  txVector.SetTxPowerLevel (m_input.txPowerLevel);
  txVector.SetMode (mode);
  mpduType mpdu = NORMAL_MPDU;

  m_node->SendPacket (p, txVector, WIFI_PREAMBLE_SHORT, mpdu);
}

void
NeighbourDiscovery::Receive (Ptr<Packet> p, double snr, WifiTxVector txVector, enum WifiPreamble preamble){
	// m_output.received++;
}

/*
 *
 * use p_senderDirection and p_senderBeamwidth for gossip
 *
 */
void NeighbourDiscovery::registerBeaconMessage(uint16_t p_senderNodeId, uint16_t p_senderSectorId, uint16_t p_replySlot , double p_senderDirection, double p_senderBeamwidth, int ack){

	// Function log
	NS_LOG_FUNCTION("NeighbourDiscovery::registerBeaconMessage");

	// Update variables
	m_beaconCount++;

	// Print receive beacon message
	NS_LOG_INFO("Node " << getId() << " (RECEIVED BEACON) << from Node " << p_senderNodeId << " in slot " <<  getTimeSlot() << " Reply slot " << p_replySlot
			<< " senderDirection " << p_senderDirection << " senderBeamwidth " << p_senderBeamwidth);

	// Update probability tree

	// Add event in priority list if sector is set

	// Set acknowledgement slot
	if(m_handshake && (ack != 1)){

		sector replySector = {}; // will zero all fields in C++
		replySector.sectorId = p_senderSectorId;
		replySector.timeSlot = p_replySlot;
		replySector.transmit = true;
		replySector.ack = true;
		replySector.direction = getDirection();
		replySector.beamwidth = getBeamwidth();

		m_priorityList.push_back(replySector);


	} else {
		NS_LOG_INFO("----- Ack is set");
	}

	// Check if neighbour is in neighbour list ...
	for (uint32_t i = 0; i < m_discovered_list.size(); ++i){
		if(m_discovered_list.at(i).nodeId == p_senderNodeId){
			return;
		}
	}

	// ... If not add
	DiscoverdNodes discovered_Node = {
			p_senderNodeId,
			getDirection(),
			getBeamwidth(),
			0.0,
			getTimeSlot()
	      };

	setDiscoveredNodes(discovered_Node);
}

// --------------------------------- Register Collision -------------------------------------------

void NeighbourDiscovery::registerCollision(Ptr<Packet> packet){

	NS_LOG_FUNCTION("NeighbourDiscovery::registerCollision");

	NS_LOG_INFO("Node " << getId() << " (Collision) timeslot: " << getTimeSlot() << " at orientation " << getDirection() << " with beamwidth " << getBeamwidth());

	m_interferenceCount++;

	// Update probability tree
	// SectorNew *sec = m_Tree_Transmit.getSector(m_sectors.at(pMSectorIndex).sectorId);
	// m_Tree_Transmit.updateSector(sec, -0.1);

}

Ptr<YansWifiPhy>
NeighbourDiscovery::CreateNode (Ptr<YansWifiChannel> channel, Ptr<ErrorRateModel> error, std::vector<NeighbourDiscovery>* m_nodesVector){

	NS_LOG_FUNCTION("NeighbourDiscovery::CreateNode");

	// Node
	Ptr<YansWifiPhy> node = CreateObject<YansWifiPhy> ();
	node->setNodesVector(m_nodesVector);

	// Setup for first run (like random orientation)
	setup = true;

	// Set dBm to 10 according to IEEE 802.15.3c
	node->SetTxPowerStart(10.0);
	node->SetTxPowerEnd(10.0);

	// Mobility
	Ptr<MobilityModel> pos = CreateObject<ConstantPositionMobilityModel> ();
	node->SetMobility(pos);

	node->SetErrorRateModel (error);
	node->SetChannel (channel);
	node->SetMobility (pos);

	// Cosine Antenna Model
	CosineAntennaModel *cosine = new CosineAntennaModel();
	node->setCosineAntennaModel(cosine);

	// Set Receive ok callback
	node->SetReceiveOkCallback (MakeCallback (&NeighbourDiscovery::Receive, this));

	m_node = node;

	return node;
}

// -------------------------------- Continuing -------------------------------------------------

/*
 * Sends beacon messages into the given direction with the given beamwidth
 * The method stops automatically after continuingTimeslotLength
 */
void NeighbourDiscovery::initializeContinuingTransmit(int continuingTimeslotLength){

	NS_LOG_FUNCTION("NeighbourDiscovery::initializeContinuingTransmit");

	int timeSlotStart = 1;

	// setDirection(getAntennaOffset());

	// logNodeInfo();

	// Schedule beacon events
	for(int i=timeSlotStart; i < continuingTimeslotLength; i++){

		int tmp_timeslot = getTimeSlot() + i;
		Simulator::Schedule(Seconds(tmp_timeslot), &NeighbourDiscovery::scheduleBeaconAt, this, (tmp_timeslot));
	}
}

/*
 * Receives beacon messages into the given direction with the given beamwidth
 * The method stops automatically after continuingTimeslotLength
 */
void NeighbourDiscovery::initializeContinuingReceive(int continuingTimeslotLength){

	NS_LOG_FUNCTION("NeighbourDiscovery::initializeContinuingReceive");

	int timeSlotStart = 1;

	// logNodeInfo();

	// Schedule receive events
	for(int i=timeSlotStart; i < continuingTimeslotLength; i++){

		int tmp_timeslot = getTimeSlot() + i;
		Simulator::Schedule(Seconds(tmp_timeslot), &NeighbourDiscovery::scheduleReceiveAt, this, (tmp_timeslot));
	}
}

// -------------------------------- Schedule Beacon/Receive at specific time slot -----------------------------

void NeighbourDiscovery::scheduleBeaconAt(int scheduleTimeslot){

	NS_LOG_FUNCTION("NeighbourDiscovery::scheduleBeaconAt");

	setTimeSlot(scheduleTimeslot);

	Simulator::Schedule(Seconds(0), &NeighbourDiscovery::Send, this);
}

void NeighbourDiscovery::scheduleReceiveAt(int scheduleSlot){

	NS_LOG_FUNCTION("NeighbourDiscovery::scheduleReceiveAt");

	setTimeSlot(scheduleSlot);

	m_receiveCount++;

	// Node listens automatically nothing to be done
}



// ----------------------------------- Node sweeps  --------------------------------

/*
 * Initializes sweep
 * A sending node transmits into each sector x beacon messages
 * x is the number a receiving node needs to sweep through all sectors
 *
 */
void NeighbourDiscovery::initializeSweep(double directionSkipCount){

	// -- Debug
	NS_LOG_FUNCTION("NeighbourDiscovery::initializeSweep");

	// -- Break Condition
	if(m_maxTimeSlot != -1 && getTimeSlot() > m_maxTimeSlot){
		// printDisoveredNeighours();
		return;
	}

	// -- Setup random direction in setup phase
	if(setup){

		// Done for evaluation purpose that direction is same to sweep

		double adjustSteer = 0;

		if(getBeamwidth() != 180){
			adjustSteer = getBeamwidth() / 2;
		}

		// Set antenna offset
		setDirection(getAntennaOffset() + adjustSteer);

		//
		setup = false;
	}

	// -- Random mode

	m_modeSend = randomMode();

	// -- Create transmit / receive events
	if(m_modeSend){
		for(int i = 1; i <= (getAntennaSectors()/directionSkipCount * getAntennaSectors()/directionSkipCount); i++){ // pow(anntennaSectors/sectorSkipCount, 2)
			Simulator::Schedule(Seconds(i), &NeighbourDiscovery::sweepTransmit, this, getTimeSlot()+i, directionSkipCount);
		}

	} else {
		for(int i = 1; i <= (getAntennaSectors()/directionSkipCount * getAntennaSectors()/directionSkipCount); i++){
			Simulator::Schedule(Seconds(i), &NeighbourDiscovery::sweepReceive, this, getTimeSlot()+i, directionSkipCount);
		}
	}

	// Loop
	Simulator::Schedule(Seconds(getAntennaSectors()/directionSkipCount * getAntennaSectors()/directionSkipCount), &NeighbourDiscovery::initializeSweep, this, directionSkipCount);
}

/*
 * Sends into a sector and changes the sector when needed
 *
 */
void NeighbourDiscovery::sweepTransmit(int pTimeSlot, double pDirectionSkipCount){

	NS_LOG_FUNCTION("NeighbourDiscovery::sweepSend");

	if(getId() == 0 && getTimeSlot() % 100 == 0){
		NS_LOG_UNCOND("Timeslot " << getTimeSlot());
	}

	// Update variables
	setTimeSlot(pTimeSlot);

	// Checks if sending node should set next sector
	if(getTimeSlot() != 1 && ((getTimeSlot()-1) % (int)(getAntennaSectors()/pDirectionSkipCount)) == 0){

		setDirection(getDirection() + pDirectionSkipCount);
	}

	logNodeInfo();

	Simulator::Schedule(Seconds(0.0), &NeighbourDiscovery::Send, this);
}

/*
 * Sweep receives through all sectors
 *
 */
void NeighbourDiscovery::sweepReceive(int pTimeSlot, double pDirectionSkipCount){

	NS_LOG_FUNCTION("NeighbourDiscovery::sweepReceiving");

	if(getId() == 0 && getTimeSlot() % 100 == 0){
		NS_LOG_UNCOND("Timeslot " << getTimeSlot());
	}

	// Update varialbes - evaluation
	m_receiveCount++;

	setTimeSlot(pTimeSlot);

	// Rotate antenna to next sector
	setDirection(getDirection() + pDirectionSkipCount);

	logNodeInfo();
}

// ---------------------------------- Adaptive ND ------------------------------------------------

/*
 *
 */
void NeighbourDiscovery::initializeAdaptive(){

	NS_LOG_FUNCTION("NeighbourDiscovery::initializeAdaptive()");

	m_beaconIntervalLength = 4;

	beaconIntervalReplySkip = 3;

	for(int i=0; i < 4; i++){
		layerCountTransmit.push_back(0);
		layerCountReceive.push_back(0);
	}

	createTree();

	topAdaptive();

}

/*
 *
 * 1. Set mode
 * 2. Choose Sectors
 * 3. Schedule beaconIntervalLength transmit/receive events
 *
 */
void NeighbourDiscovery::topAdaptive(){

	NS_LOG_FUNCTION("NeighbourDiscovery::topAdaptive()");

	// Break and print
	if(m_maxTimeSlot != -1 && getTimeSlot() > m_maxTimeSlot){
		// printDisoveredNeighours();
		return;
	}

	if(!m_fixPriorityMode){

		// reset
		m_fixPriorityMode = false;
		m_sectors.clear();

		// random state

		if(getId() == 0){
			NS_LOG_DEBUG("Node 0 - SET FIX TRANSMIT");
		}

		if(m_handshake){

			if(getTimeSlot() == 0){
				m_modeSend = randomMode();
			} else {
				m_modeSend = true;
			}

		} else {
			m_modeSend = randomMode();
		}

		freePriorityListIndex = returnRandomAvailableBeaconInterval();

		// fill interval
		fillBeaconInterval();
	}

	// schedule transmit / receive events
	scheduleBeaconInterval();
}

/*
 * Select random sectors from the tree and fill the sector list for the next beacon interval
 *
 */
void NeighbourDiscovery::fillBeaconInterval(){

	for(int i = 1; i <= m_beaconIntervalLength; i++){

		// Initialize sector
		sector transmitSectorInformation = {};
		SectorNew* treeSector = new SectorNew();

		// Get most probable sectors
		if(m_modeSend){
			treeSector = m_Tree_Transmit.getMostProbableSector();
			if(getId() == 0){
				// NS_LOG_UNCOND("Chosen sector " << treeSector->id);
			}
		} else {
			treeSector = m_Tree_Receive.getMostProbableSector();
			if(getId() == 0){
				// NS_LOG_UNCOND("Chosen sector " << treeSector->id);
			}
		}

		// Copy treeSector to sectorTransmit
		transmitSectorInformation.sectorId = treeSector->id;
		transmitSectorInformation.ack = false;
		transmitSectorInformation.direction = treeSector->direction;
		transmitSectorInformation.beamwidth = treeSector->beamwidth;

		m_sectors.push_back(transmitSectorInformation);
	}
}

void NeighbourDiscovery::scheduleBeaconInterval(){
	// Schedule transmit and receive events - sectors are already filled
	for(int i = 1; i <= m_beaconIntervalLength; i++){

		int timeslotForSector = getTimeSlot() + i;

		// Schedule transmit or receive events
		if(m_modeSend){
			NS_LOG_DEBUG("	Set Transmit Beacon (fix): Timeslot " << timeslotForSector);
			Simulator::Schedule(Seconds(i), &NeighbourDiscovery::adaptiveTransmit, this, (i-1), timeslotForSector);
		} else {
			NS_LOG_DEBUG("	Set Receive Beacon (fix): Timeslot  " << timeslotForSector);
			Simulator::Schedule(Seconds(i), &NeighbourDiscovery::adaptiveReceive, this, (i-1), timeslotForSector);
		}
	}
}

/*
 * Adaptive transmit, sends beacon packets into a certain sector and the given timepoint
 * After sending, the probability tree is updated and a listen event is added to the
 * priority list
 *
 * Update tree (value is set in this method)
 */
void NeighbourDiscovery::adaptiveTransmit(int pMSectorIndex, int pSectorTimeslot){

	NS_LOG_FUNCTION("NeighbourDiscovery::adaptiveTransmit()");

	if(getId() == 0 && getTimeSlot() % 100 == 0){
		NS_LOG_UNCOND("Timeslot " << getTimeSlot());
	}

	double beamwidth = m_sectors.at(pMSectorIndex).beamwidth;
	double direction = m_sectors.at(pMSectorIndex).direction;

	// Set sector information
	setBeamwidth(beamwidth);
	setDirection(direction);

	logNodeInfo();

	// Update
	setTimeSlot(pSectorTimeslot);

	// Evaluation
	evaluateLayerCountTransmit(getBeamwidth());

	// Set reply slot
	int replySlot = getTimeSlot() + freePriorityListIndex * m_beaconIntervalLength;

	Simulator::Schedule(Seconds(0.0), &NeighbourDiscovery::SendWithInfo, this, pMSectorIndex, replySlot);

	if(m_handshake){
		addToPriorityList(m_sectors.at(pMSectorIndex).sectorId, replySlot);
	}

	// Check priority list
	if(pMSectorIndex == (m_beaconIntervalLength-1)){
		Simulator::Schedule(Seconds(0), &NeighbourDiscovery::adaptiveCheckPriority, this);
	}
}

/*
 *
 *
 */
void NeighbourDiscovery::adaptiveReceive(int pMSectorIndex, int p_sectorTimeSlot){

	NS_LOG_FUNCTION("NeighbourDiscovery::adaptiveReceive()");

	if(getId() == 0 && getTimeSlot() % 100 == 0){
		NS_LOG_UNCOND("Timeslot " << getTimeSlot());
	}

	// Set sector information
	setBeamwidth(m_sectors.at(pMSectorIndex).beamwidth);
	setDirection(m_sectors.at(pMSectorIndex).direction);

	// Update variables
	m_receiveCount++;
	setTimeSlot(p_sectorTimeSlot);

	// Evaluation
	logNodeInfo();
	evaluateLayerCountReceive(getBeamwidth());

	// Check priority list
	if(pMSectorIndex == (m_beaconIntervalLength-1)){
		Simulator::Schedule(Seconds(0), &NeighbourDiscovery::adaptiveCheckPriority, this);
	}
}

void NeighbourDiscovery::sortPriorityList(){

	// Sort priority list
	std::vector<sector> tmpPriorityList;

	while((int)m_priorityList.size() > 0){
		int minTimeSlot = -1;
		int index = -1;

		for(int i=0; i<(int)m_priorityList.size(); i++){
			if(minTimeSlot == -1 || minTimeSlot > m_priorityList.at(i).timeSlot){
				minTimeSlot = m_priorityList.at(i).timeSlot;
				index = i;
			}
		}

		tmpPriorityList.push_back(m_priorityList.at(index));
		m_priorityList.erase(m_priorityList.begin() + index);
	}

	for(int i=0; i<(int)tmpPriorityList.size(); i++){
		m_priorityList.push_back(tmpPriorityList.at(i));
	}
}


//Element 0 : (receive) timeslot 149 direction 216.25 beamwidth 22.5
//Element 1 : (receive) timeslot 150 direction 238.75 beamwidth 22.5
//Element 2 : (receive) timeslot 151 direction 115 beamwidth 90
//Element 3 : (transmit) timeslot 151 direction 295 beamwidth 90
//Element 4 : (receive) timeslot 152 direction 182.5 beamwidth 45
//Element 5 : (receive) timeslot 153 direction 295 beamwidth 90
//Element 6 : (receive) timeslot 154 direction 193.75 beamwidth 22.5
//Element 7 : (receive) timeslot 155 direction 148.75 beamwidth 22.5
//Element 8 : (receive) timeslot 156 direction 328.75 beamwidth 22.5


void NeighbourDiscovery::bringTransmitSectorsInFront(){

	if(getId() == 0){
		NS_LOG_DEBUG("NeighbourDiscovery::bringTransmitSectorsInFront()");
	}

	std::vector<sector> tmpPriorityList;

	for(int i=0; i<(int)m_priorityList.size(); i++){
		tmpPriorityList.push_back(m_priorityList.at(i));
	}

	m_priorityList.erase(m_priorityList.begin(), m_priorityList.end());

	// Remove receive events if reply message is in priority list
	int beaconIntervalStart = getTimeSlot() + 1;

	for(int i=0; i < 3; i++){

		bool transmitSector = false;

		for(int i=0; i<(int)tmpPriorityList.size(); i++){
			if(tmpPriorityList.at(i).timeSlot >= beaconIntervalStart && tmpPriorityList.at(i).timeSlot < beaconIntervalStart + (int) m_beaconIntervalLength && tmpPriorityList.at(i).transmit){
				if(getId() == 0){
					NS_LOG_DEBUG("BeaconInterval " << i << " transmit slot");
				}
				transmitSector = true;
			}
		}

		for(int i=0; i<(int)tmpPriorityList.size(); i++){
			if(transmitSector){ // Add transmit sector (ignore receive sector)
				if(tmpPriorityList.at(i).timeSlot >= beaconIntervalStart && tmpPriorityList.at(i).timeSlot < beaconIntervalStart + (int) m_beaconIntervalLength && tmpPriorityList.at(i).transmit){
					if(getId() == 0){
						NS_LOG_DEBUG("Add element to priority list (transmit) " << tmpPriorityList.at(i).timeSlot);
					}
					m_priorityList.push_back(tmpPriorityList.at(i));
				}
			} else { // no transmit sectors, add receive sectors
				if(tmpPriorityList.at(i).timeSlot >= beaconIntervalStart && tmpPriorityList.at(i).timeSlot < beaconIntervalStart + (int) m_beaconIntervalLength){
					if(getId() == 0){
						NS_LOG_DEBUG("Add element to priority list (receive) " << tmpPriorityList.at(i).timeSlot);
					}
					m_priorityList.push_back(tmpPriorityList.at(i));
				}
			}
		}

		beaconIntervalStart += (int) m_beaconIntervalLength;
	}
}

/*
 * This method checks if there are events in the priority list
 * If so the mode is fix set and the events in the priority list
 * are added to the priority list. Not set sectors are added randomly
 *
 */
void NeighbourDiscovery::adaptiveCheckPriority(){

	// Function log
	NS_LOG_FUNCTION("NeighbourDiscovery::adaptiveCheckPriority()");

	sortPriorityList();

	bringTransmitSectorsInFront();

	// Print priority list to LOG
	if(getId() == 0){
		NS_LOG_DEBUG("Node " << getId() << " interval to fill " << getTimeSlot()+1 << "-" << getTimeSlot()+m_beaconIntervalLength  << " priority list (size=" << m_priorityList.size() << ") ");

		for (int i=0; i < (int)m_priorityList.size(); i++) {

			if(m_priorityList.at(i).transmit){
				NS_LOG_DEBUG("	Element " << i << " : (transmit) timeslot " << m_priorityList.at(i).timeSlot << " direction " << m_priorityList.at(i).direction << " beamwidth " << m_priorityList.at(i).beamwidth);
			} else {
				NS_LOG_DEBUG("	Element " << i << " : (receive) timeslot " << m_priorityList.at(i).timeSlot << " direction " << m_priorityList.at(i).direction << " beamwidth " << m_priorityList.at(i).beamwidth);
			}
		}
	}

	// End print priority list to LOG

	// Reset sector list and fixPriorityMode
	m_sectors.clear();
	m_fixPriorityMode = false;

	// Check if element in priority list need to be addresses
	// If so, set mode and fix fixPriorityMode
	for(uint16_t i=0; i < m_beaconIntervalLength; i++){
		if(m_priorityList.size() > i){
			// current sector
			sector tmp_sec = m_priorityList.at(i);

			// check if sector fits into next beacon time interval
			if(tmp_sec.transmit && !m_fixPriorityMode && tmp_sec.timeSlot <= (getTimeSlot() + m_beaconIntervalLength)){
				m_modeSend = true;
				m_fixPriorityMode = true;
				NS_LOG_DEBUG("Node " << getId() << " (fix mode = transmit)");
			} else if (!tmp_sec.transmit && !m_fixPriorityMode && tmp_sec.timeSlot <= (getTimeSlot() + m_beaconIntervalLength)){
				m_modeSend = false;
				m_fixPriorityMode = true;
				NS_LOG_DEBUG("Node " << getId() << " (fix mode = receive)");
			}
		}
	}

	// Mode is set, fill sector list with the sectors

	int priorityListNextElementIndex = 0;

	if(m_fixPriorityMode){
		for(uint16_t beaconIntervalIndex=0; beaconIntervalIndex < m_beaconIntervalLength; beaconIntervalIndex++){

			sector tmp = {};
			tmp.sectorId = -1;

			if((int) m_priorityList.size() > priorityListNextElementIndex){
				// tmp = m_priorityList.at(beaconIntervalIndex); // priority list element

				tmp.sectorId = m_priorityList.at(priorityListNextElementIndex).sectorId;
				tmp.transmit = m_priorityList.at(priorityListNextElementIndex).transmit;
				tmp.timeSlot = m_priorityList.at(priorityListNextElementIndex).timeSlot;
				tmp.ack = m_priorityList.at(priorityListNextElementIndex).ack;
				tmp.direction = m_priorityList.at(priorityListNextElementIndex).direction;
				tmp.beamwidth = m_priorityList.at(priorityListNextElementIndex).beamwidth;

			} else {
				// tmp empty
			}

			// If true, set priority list element, else set random element

			if(tmp.sectorId != -1 && tmp.timeSlot == getTimeSlot() + beaconIntervalIndex + 1){ // +1 since beaconIntervalIndex starts with 0

				if(tmp.transmit){
					NS_LOG_DEBUG("SET ACK TO TRUE");
				}

				// Add element from priority list to sectors
				m_sectors.push_back(tmp);

				priorityListNextElementIndex++;

			// If no element in priority list, add random element
			} else {

				if(m_modeSend){
					sector tmpSector = {}; // will zero all fields in C++
					SectorNew* sector = m_Tree_Transmit.getMostProbableSector(); // get sector from tree
					tmpSector.sectorId = sector->id;
					tmpSector.transmit = true;
					tmpSector.ack = false;
					tmpSector.direction = sector->direction;
					tmpSector.beamwidth = sector->beamwidth;
					m_sectors.push_back(tmpSector);
					//NS_LOG_UNCOND("----- sector random from tree " << tmpSector.timeSlot << " direction " <<
					//		tmpSector.direction << " beamwidth " << tmpSector.beamwidth );
				} else {
					sector tmpSector = {}; // will zero all fields in C++
					SectorNew* sector = m_Tree_Receive.getMostProbableSector(); // get sector from tree
					tmpSector.sectorId = sector->id;
					tmpSector.transmit = false;
					tmpSector.ack = false;
					tmpSector.direction = sector->direction;
					tmpSector.beamwidth = sector->beamwidth;
					m_sectors.push_back(tmpSector);
					//NS_LOG_UNCOND("----- sector random from tree " << tmpSector.timeSlot << " direction " <<
					//		tmpSector.direction << " beamwidth " << tmpSector.beamwidth );
				}
			}
		}
	}

	// Remove old priority elements
	uint16_t eraseIndex = 0;

	// Find the index to which all elements in the priority list are erased
	NS_LOG_DEBUG("Node " << getId() << " Priority list size " << m_priorityList.size());
	for(uint16_t i=0; i < m_priorityList.size(); i++){
		if(m_priorityList.at(i).timeSlot <= (getTimeSlot() + (uint16_t)m_beaconIntervalLength)){
			eraseIndex = i + 1;
		}
	}

	// Erase elements from the priority list
	if(eraseIndex > 0){
		m_priorityList.erase(m_priorityList.begin(), m_priorityList.begin() + eraseIndex);
	}

	NS_LOG_INFO("Node " << getId() << " filled mSector list (size =" << m_sectors.size() <<" )");

	for (int i=0; i < (int)m_sectors.size(); i++) {

		if(m_sectors.at(i).transmit){
			NS_LOG_INFO("	MSectors Element " << i << " : (transmit) timeslot " << m_sectors.at(i).timeSlot << " direction " << m_sectors.at(i).direction << " beamwidth " << m_sectors.at(i).beamwidth);
		} else {
			NS_LOG_INFO("	MSectors Element " << i << " : (receive) timeslot " << m_sectors.at(i).timeSlot << " direction " << m_sectors.at(i).direction << " beamwidth " << m_sectors.at(i).beamwidth);
		}
	}

	// Jump back to topAdaptive
	topAdaptive();
}

/*
 * Add sector to priority list
 *
 */
void NeighbourDiscovery::addToPriorityList(int sectorId, int replySlot){
	sector listenSecotInformation = {}; // will zero all fields in C++
	listenSecotInformation.sectorId = sectorId;
	listenSecotInformation.timeSlot = replySlot;
	listenSecotInformation.transmit = false;
	listenSecotInformation.ack = false;
	listenSecotInformation.direction = getDirection();
	listenSecotInformation.beamwidth = getBeamwidth();

	// Add sector to priority list
	m_priorityList.push_back(listenSecotInformation);

}

int NeighbourDiscovery::returnRandomAvailableBeaconInterval(){
	// Priority list index 1 ... 3

	std::vector<int> blockedIndex;

	if(getId() == 0){
		NS_LOG_DEBUG("returnRandomAvailableBeaconInterval timeslot " <<getTimeSlot());
	}

	for(int i=0; i<(int)m_priorityList.size(); i++){
		NS_LOG_DEBUG("Element " << i << " timeslot " << m_priorityList.at(i).timeSlot);
	}

	for(int i=0; i < (int) m_priorityList.size(); i++){ // Find blocked index

		if(getId() == 0){
			NS_LOG_DEBUG("priority list timeslot " << m_priorityList.at(i).timeSlot << " timeslot von " << getTimeSlot()+1 << " bis " << getTimeSlot()+12);
		}

		if(m_priorityList.at(i).timeSlot == getTimeSlot() + 5 || m_priorityList.at(i).timeSlot == getTimeSlot() + 6  || m_priorityList.at(i).timeSlot == getTimeSlot() + 7 || m_priorityList.at(i).timeSlot == getTimeSlot() + 8){
			if ( std::find(blockedIndex.begin(), blockedIndex.end(), 1) != blockedIndex.end()){} else {blockedIndex.push_back(1);}
		}
		if(m_priorityList.at(i).timeSlot == getTimeSlot() + 9 || m_priorityList.at(i).timeSlot == getTimeSlot() + 10  || m_priorityList.at(i).timeSlot == getTimeSlot() + 11 || m_priorityList.at(i).timeSlot == getTimeSlot() + 12){
			if ( std::find(blockedIndex.begin(), blockedIndex.end(), 2) != blockedIndex.end()){} else {blockedIndex.push_back(2);}
		}
		if(m_priorityList.at(i).timeSlot == getTimeSlot() + 13 || m_priorityList.at(i).timeSlot == getTimeSlot() + 14  || m_priorityList.at(i).timeSlot == getTimeSlot() + 15 || m_priorityList.at(i).timeSlot == getTimeSlot() + 16){
			if ( std::find(blockedIndex.begin(), blockedIndex.end(), 3) != blockedIndex.end()){} else {blockedIndex.push_back(3);}
		}

	}

	if(getId() == 0){
		NS_LOG_DEBUG("blockedIndex size " << blockedIndex.size());

		for(int i=0; i<(int)blockedIndex.size(); i++){
			NS_LOG_DEBUG("blockedIndex " << blockedIndex.at(i));
		}
	}


	std::vector<int> availableIndex; // 1 ... 3

	for(int item = 1; item <= 3; item++){ // remove blocked index
		if ( std::find(blockedIndex.begin(), blockedIndex.end(), item) != blockedIndex.end() ){
			// nothing
		} else {
			availableIndex.push_back(item);
		}
	}

	if(getId() == 0){
		NS_LOG_DEBUG("availableIndex size " << availableIndex.size());

		for(int i=0; i<(int)availableIndex.size(); i++){
			NS_LOG_DEBUG("availableIndex " << availableIndex.at(i));
		}
	}

	// choose randomly between available index
	int randomIndex = rand() % (int) availableIndex.size();
	int index = availableIndex.at(randomIndex);

	if(getId() == 0){
		NS_LOG_DEBUG("Index " << index);
	}

	return index;
}

/*
 * Layer count transmit
 */
void NeighbourDiscovery::evaluateLayerCountTransmit(double beamwidth){
	if(beamwidth == 180){
		layerCountTransmit.at(0) += 1;
	} else if(beamwidth == 90){
		layerCountTransmit.at(1) +=  1;
	} else if(beamwidth == 45) {
		layerCountTransmit.at(2) +=  1;
	} else if(beamwidth == 22.5){
		layerCountTransmit.at(3) += 1;
	}
}

/*
 * Layer count receive
 */
void NeighbourDiscovery::evaluateLayerCountReceive(double beamwidth){
	if(beamwidth == 180){
		layerCountReceive.at(0) += 1;
	} else if(beamwidth == 90){
		layerCountReceive.at(1) += 1;
	} else if(beamwidth == 45) {
		layerCountReceive.at(2) += 1;
	} else if(beamwidth == 22.5){
		layerCountReceive.at(3) += 1;
	}
}

/*
 * Creates a probability tree a node uses to chose
 * transmit and receive sectors, this is quite ugly
 *
 */
void NeighbourDiscovery::createTree(){

	// Function log
	NS_LOG_FUNCTION("NeighbourDiscovery::createTree");

	std::vector<uint16_t> childrenPerNode;
	childrenPerNode.push_back(2);
	childrenPerNode.push_back(2);
	childrenPerNode.push_back(2);
	childrenPerNode.push_back(2);

	// Defines the beamwidth per layer
	std::vector<double> beamwidthPerLayer;
	beamwidthPerLayer.push_back(180.0);
	beamwidthPerLayer.push_back(90.0);
	beamwidthPerLayer.push_back(45.0);
	beamwidthPerLayer.push_back(22.5);

	// Defines the sector offset per layer
	std::vector<double> directionOffsetPerLayer;
	directionOffsetPerLayer.push_back(0.0);
	directionOffsetPerLayer.push_back(45.0);
	directionOffsetPerLayer.push_back(22.5); // sector offset
	directionOffsetPerLayer.push_back(11.25); // sector offset

	// Creates a transmit and receive probability tree
	m_Tree_Transmit.createTree(childrenPerNode, beamwidthPerLayer, directionOffsetPerLayer, getAntennaOffset());
	m_Tree_Receive.createTree(childrenPerNode, beamwidthPerLayer, directionOffsetPerLayer, getAntennaOffset());

	// Set sectors with 180 degree beamwidth probability to 0

	m_Tree_Transmit.resetProbabilityValue(m_Tree_Transmit.getSector(1), 0);
	m_Tree_Transmit.resetProbabilityValue(m_Tree_Transmit.getSector(2), 0);

	m_Tree_Receive.resetProbabilityValue(m_Tree_Receive.getSector(1), 0);
	m_Tree_Receive.resetProbabilityValue(m_Tree_Receive.getSector(2), 0);

	// 90 degree
	for(int i = 3 ; i <=6; i++){
		m_Tree_Transmit.resetProbabilityValue(m_Tree_Transmit.getSector(i), 0.1);
		m_Tree_Receive.resetProbabilityValue(m_Tree_Receive.getSector(i), 0.1);
	}

	// 45 degree
	for(int i = 7 ; i <=14; i++){
		m_Tree_Transmit.resetProbabilityValue(m_Tree_Transmit.getSector(i), 0.22222);
		m_Tree_Receive.resetProbabilityValue(m_Tree_Receive.getSector(i), 0.22222);
	}

	if(getId() == 0){
		m_Tree_Transmit.printTree();
	}
}

/*
 * fRand returns a double between fMin and fMax
 *
 */
double
NeighbourDiscovery::fRand(double fMin, double fMax)
{
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}


/*
 * Prints the list of discovered neighbours
 *
 */
void NeighbourDiscovery::printDisoveredNeighours(){

	// Function log
	NS_LOG_FUNCTION("NeighbourDiscovery::printDisoveredNeighours");

	// Print node id and the number of discovered neighbours
	NS_LOG_UNCOND("----- Node Id " << getId() << " ----- print discovered neighours");
	NS_LOG_UNCOND("Discovered neighours #" << getDiscoveredNodesList().size());

	// Print discovered nodes list (id, timeslot)
	for (int i = 0; i < (int )getDiscoveredNodesList().size(); ++i){
		NS_LOG_UNCOND("Node " << getId() <<" discovered node " << getDiscoveredNodesList().at(i).nodeId << " in timeslot  " <<  getDiscoveredNodesList().at(i).slot);
	}

	// Print end instruction
	NS_LOG_UNCOND("-----------------------------------------------------------------");
}

void NeighbourDiscovery::logNodeInfo(){
	if(getId() == 0){
		if(m_modeSend){
			NS_LOG_DEBUG("Node " << getId() << "("<< Simulator::Now() << ")" << " (timeslot:" << getTimeSlot() << " x:" << getXPos() << " y:" << getYPos() << ") : mode=transmit : direction " << getDirection() << " beamwidth " << getBeamwidth());
		} else {
			NS_LOG_DEBUG("Node " << getId() << "("<< Simulator::Now() << ")" << " (timeslot:" << getTimeSlot() << " x:" << getXPos() << " y:" << getYPos() << ") : mode=receive : direction " << getDirection() << " beamwidth " << getBeamwidth());
		}
	}
}

// random sector / directin
double NeighbourDiscovery::randomSector(){
  return rand() % getAntennaSectors();
}
double NeighbourDiscovery::randomDirection(){
  return rand() % 360;
}

// convert sector to direction and other way around
double NeighbourDiscovery::sectorToDirection(double sector){
  return sector * (360 / getAntennaSectors());
}
double NeighbourDiscovery::directionToSector(double direction){
  double rest = fmod(direction, (360/getAntennaSectors()));
  return (direction - rest) / (360/getAntennaSectors());
}

bool NeighbourDiscovery::randomMode(){

	Ptr<UniformRandomVariable> uv = CreateObject<UniformRandomVariable> ();
	double mode = uv->GetValue(0.0, 1.0);

	// double mode = fRand(0.0, 1.0);

	if(m_sendReceiveProbability < mode){
		return false;
	} else {
		return true;
	}
}

bool NeighbourDiscovery::randomModeAdaptive(){

	if(m_transmitCount == 0 || m_receiveCount == 0){
		return randomMode();
	}

	double rand = fRand(0.0, 1.0);

	double transmitRatio = (double)m_transmitCount / ((double)m_transmitCount + (double)m_receiveCount);

	if(rand > transmitRatio){
		return true;
	} else {
		return false;
	}
}



void NeighbourDiscovery::setBeamwidthDirectionTimeslot(double pBeamwidth, double pDirection, int pTimeslot){
  setBeamwidth(pBeamwidth);
  setDirection(pDirection);
  setTimeSlot(pTimeslot);
}

}


