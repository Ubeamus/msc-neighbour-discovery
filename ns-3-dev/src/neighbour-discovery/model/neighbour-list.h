/*
 * neighbour-list.h
 *
 *  Created on: Oct 28, 2015
 *      Author: johannes
 */

#ifndef NEIGHBOUR_LIST_H_
#define NEIGHBOUR_LIST_H_

#include <iostream>
#include <string>
using namespace std;

namespace ns3 {

class NeighbourList {
public:
	NeighbourList():head(NULL), tail(NULL){} // constructor
	virtual ~NeighbourList(); // destructor

	struct node {
			int id;
			string name;
			struct node *next;
		} *head, *tail, *ptr;

	struct NeighbourList::node* initNode(string, int);

	void reverse();
	void addNode( struct NeighbourList::node*);
	void insertNode( struct NeighbourList::node*);
	void deleteNode( struct NeighbourList::node*);
	void deleteList( struct NeighbourList::node*);
	void displayList( struct NeighbourList::node*)const ;
	void displayNode( struct NeighbourList::node*) const;
};

} /* namespace ns3 */

#endif /* NEIGHBOUR_LIST_H_ */
