/*
 * sweepND.cc
 *
 *  Created on: Oct 28, 2015
 *      Author: johannes
 */

#include "sweep-ND.h"
#include "ns3/log.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("SweepND");


SweepND::SweepND() {
	// TODO Auto-generated constructor stub
	m_numNodes = 0;
	m_areaLength = 0;
	m_areaWidth = 0;

}

SweepND::~SweepND() {
	// TODO Auto-generated destructor stub
}

// Getter
uint16_t const& SweepND::getNumNodes() const {
	return m_areaLength; }

uint16_t const& SweepND::getAreaLength() const {
	return m_areaLength; }

uint16_t const& SweepND::getAreaWidth() const {
	return m_areaLength; }

// Setter
void SweepND::setNumNodes(uint16_t const& newNumNodes) {
	m_numNodes = newNumNodes; }

void SweepND::setAreaLength(uint16_t const& newNumNodes) {
	m_areaLength = newNumNodes; }

void SweepND::setAreaWidth(uint16_t const& newNumNodes) {
	m_areaWidth = newNumNodes; }

void SweepND::startSweepND(){
	//int numNodes = getNumNodes();

	// Choose node
	for (uint16_t i = 0; i < 2; ++i)
	{
		/*
		NeighbourDiscovery node = m_nodesList.pop_back();

		for (uint32_t i = 0; i < 360; ++i){
			node.setOrientation(i);
			node.Send();
		}
		*/
	}
	// Sweep send beacon

}
void SweepND::stopSweepND(){

}

void SweepND::setNeighbourDiscoveryList(std::list<NeighbourDiscovery> list){
	m_nodesList = list;
}

} /* namespace ns3 */
