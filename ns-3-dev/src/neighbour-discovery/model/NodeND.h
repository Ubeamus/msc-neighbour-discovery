/*
 * NodeND.h
 *
 *  Created on: Nov 15, 2015
 *      Author: johannes
 */

#ifndef NODEND_H_
#define NODEND_H_

#include "ns3/wifi-net-device.h"
#include "ns3/yans-wifi-channel.h"
// #include "ns3/yans-wifi-phy.h"
#include "ns3/propagation-loss-model.h"
#include "ns3/propagation-delay-model.h"
#include "ns3/error-rate-model.h"
#include "ns3/yans-error-rate-model.h"
#include "ns3/ptr.h"
#include "ns3/mobility-model.h"
#include "ns3/constant-position-mobility-model.h"
#include "ns3/vector.h"
#include "ns3/packet.h"
#include "ns3/simulator.h"
#include "ns3/nstime.h"
#include "ns3/command-line.h"
#include "ns3/flow-id-tag.h"
#include "ns3/wifi-tx-vector.h"
#include "ns3/cosine-antenna-model.h"
#include "NDPacket.h"

namespace ns3 {

class NodeND {
public:

	struct Input
	  {
		Input ();
		std::string txMode;
		uint8_t txPowerLevel;
		uint32_t packetSize;
	  };
	  struct Output
	  {
		uint32_t received;
	  };
	  struct DiscoverdNodes
	  {
		  uint32_t nodeId;
		  double discovered_orientation;
		  double discovered_beamwidth;
		  double discoverd_snr;
	  };

	NodeND();
	virtual ~NodeND();

	// Create node
	Ptr<YansWifiPhy> CreateNode(Ptr<YansWifiChannel> channel, Ptr<ErrorRateModel> error, std::vector<NodeND* >* m_nodesVector);

	// Send / Receive
	void Send (void);
	void Receive (Ptr<Packet> p, double snr, WifiTxVector txVector, enum WifiPreamble preamble);

	virtual void Run();

	// Create Discovered Neighbour
	void createDiscoveredNeighbour(double p_orientation, double p_beamwidth, uint16_t senderNodeId);

	// Helper functions
	std::string intToString(int pValue);
	std::string doubleToString(double pValue);

	const double toDegree(const double radian);
	const double toRadian(const double degree);

	double adjustGain(double pBeamwidth);

	// Getter
	int getId(); // ID
	double getXPos(); // Position
	double getYPos();
	double getBeamwidth(); // Beamwidth / Orientation
	double getOrientation();
	double getTxGain(); // txGain
	double getRxGain(); // rxGain

	Ptr<YansWifiPhy> getWifiPhy(); // get Phy

	std::vector<DiscoverdNodes* > getDiscoveredNodesList();

	// Setter
	void setId(int xPos);
	void setXPos(double xPos); // Position
	void setYPos(double yPos);
	void setBeamwidth(double beamwidth); // Orientation
	void setOrientation(double orientation);
	void setTxGain(double txGain); // txGain
	void setRxGain(double rxGain); // rxGain


void setDiscoveredNodes (DiscoverdNodes* node);

	Ptr<YansWifiPhy> m_node; // Phy

	uint16_t m_id; // Id

	struct Input m_input;
	struct Output m_output;

	std::vector< DiscoverdNodes* > m_discovered_list;

private:


};

} /* namespace ns3 */

#endif /* NODEND_H_ */
