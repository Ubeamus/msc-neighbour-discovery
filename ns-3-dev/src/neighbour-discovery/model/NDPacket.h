/*
 * NDPacket.h
 *
 *  Created on: Nov 3, 2015
 *      Author: johannes
 */

#ifndef NDPACKET_H_
#define NDPACKET_H_

#include "ns3/header.h"
#include "ns3/ptr.h"
#include "ns3/log.h"
#include "ns3/vector.h"
#include "ns3/packet.h"
#include "ns3/simulator.h"
#include "ns3/command-line.h"
#include "ns3/flow-id-tag.h"
#include "SectorInformation.h"
#include "GossipInformation.h"


namespace ns3 {

class NDPacket : public Header
{
public:

  NDPacket ();
  virtual ~NDPacket ();

  void SetId (uint16_t data);
  uint16_t GetId (void) const;

  void SetSectorInformation (uint16_t p_SectorId);
  uint16_t GetSectorInformation (void) const;

  void SetReplySlots (uint16_t slot);
  uint16_t GetReplySlots (void) const;

  void SetBeamwidth (uint16_t beamwidth);
  uint16_t GetBeamwidth (void) const;

   void SetDirection (uint16_t direction);
   uint16_t GetDirection(void) const;

  void SetAck (int data);
  int GetAck (void) const;

  void SetGossipInformation (std::vector<GossipInformation> changeMethod);
  std::vector<GossipInformation> GetGossipInformation (void) const;

  static TypeId GetTypeId (void);
  virtual TypeId GetInstanceTypeId (void) const;
  virtual void Print (std::ostream &os) const;
  virtual void Serialize (Buffer::Iterator start) const;
  virtual uint32_t Deserialize (Buffer::Iterator start);
  virtual uint32_t GetSerializedSize (void) const;

private:
  uint16_t m_id;
  uint16_t m_ack;
  uint16_t m_sectorId;
  uint16_t m_replySlot;
  uint16_t m_direction;
  uint16_t m_beamwidth;

  std::vector<GossipInformation> m_gossip;
};

} /* namespace ns3 */

#endif /* NDPACKET_H_ */
