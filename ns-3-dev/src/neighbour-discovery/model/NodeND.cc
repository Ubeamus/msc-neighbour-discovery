/*
 * NodeND.cpp
 *
 *  Created on: Nov 15, 2015
 *      Author: johannes
 */

#include "NodeND.h"
#include "ns3/log.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("NodeND");

NodeND::NodeND() {
	// TODO Auto-generated constructor stub

}

NodeND::~NodeND() {
	// TODO Auto-generated destructor stub
}

NodeND::Input::Input ()
  : txMode ("OfdmRate6Mbps"),
    txPowerLevel (0),
    packetSize (2304) {}

std::string NodeND::intToString(int pValue){
	std::ostringstream strs;
	strs << pValue;
	return strs.str();}

std::string NodeND::doubleToString(double pValue){
	std::ostringstream strs;
	strs << pValue;
	return strs.str();}

const double NodeND::toDegree(const double radian){
	return radian * 180.0 / 3.14159265; }

const double NodeND::toRadian(const double degree){
	return degree * 3.14159265 / 180.0; }

double
NodeND::adjustGain(double hpbw_InRadian){
	return 10*log10(pow((1.6162/sin(hpbw_InRadian/2)),2));
}
// Getter
double NodeND::getXPos() {
	return m_node->GetMobility()->GetPosition().x;
}
double NodeND::getYPos() {
	return m_node->GetMobility()->GetPosition().y;
}
double NodeND::getBeamwidth() {
	return m_node->getCosineAntennaModel()->GetBeamwidth();
}
double NodeND::getOrientation() {
	return m_node->getCosineAntennaModel()->GetOrientation();
}
int NodeND::getId() {
	return m_id;
}
std::vector<NodeND::DiscoverdNodes* > NodeND::getDiscoveredNodesList(){
	return m_discovered_list;
}
Ptr<YansWifiPhy> NodeND::getWifiPhy(){
	return m_node;
}


// Setter
void NodeND::setXPos(double xPos){
	m_node->GetMobility()->SetPosition(Vector (xPos, getYPos(), 0.0));
}
void NodeND::setYPos(double yPos){
	m_node->GetMobility()->SetPosition(Vector (getXPos(), yPos, 0.0));
}
void NodeND::setBeamwidth(double beamwidth){
	// m_beamwidth = beamwidth;
	m_node->getCosineAntennaModel()->SetBeamwidth(beamwidth);
}
void NodeND::setOrientation(double orientation){
	// m_orientation = orientation;
	m_node->getCosineAntennaModel()->SetOrientation(orientation);
}
void NodeND::setId(int id){
	m_id = id;
	m_node->setId(id);
}

void NodeND::setTxGain(double hpbw){
	m_node->SetTxGain(adjustGain(toRadian(hpbw)));
}

void NodeND::setRxGain(double hpbw){
	m_node->SetRxGain(adjustGain(toRadian(hpbw)));
}

void NodeND::setDiscoveredNodes (DiscoverdNodes* node){
	m_discovered_list.push_back(node);
}

void
NodeND::Send (void){
  NS_LOG_UNCOND("Run");
  Ptr<Packet> p = Create<Packet> (m_input.packetSize);

  // Add NDPacket
  NDPacket ndPacket;
  ndPacket.SetData(m_id);
  p->AddHeader(ndPacket);

  WifiMode mode = WifiMode (m_input.txMode);
  WifiTxVector txVector;
  txVector.SetTxPowerLevel (m_input.txPowerLevel);
  txVector.SetMode (mode);
  mpduType mpdu = NORMAL_MPDU;
  m_node->SendPacket (p, txVector, WIFI_PREAMBLE_SHORT, mpdu);
}

void
NodeND::Receive (Ptr<Packet> p, double snr, WifiTxVector txVector, enum WifiPreamble preamble){
  m_output.received++;
}

Ptr<YansWifiPhy>
NodeND::CreateNode (Ptr<YansWifiChannel> channel, Ptr<ErrorRateModel> error, std::vector<NodeND* >* m_nodesVector){

	// Node
	Ptr<YansWifiPhy> node = CreateObject<YansWifiPhy> ();
	node->setNodesVector(m_nodesVector);

	// Set dbM to 10 according to IEEE 802.15.3c
	node->SetTxPowerStart(10.0);
	node->SetTxPowerEnd(10.0);

	// Mobility
	Ptr<MobilityModel> pos = CreateObject<ConstantPositionMobilityModel> ();
	node->SetMobility(pos);

	node->SetErrorRateModel (error);
	node->SetChannel (channel);
	node->SetMobility (pos);

	// Cosine Antenna Model
	CosineAntennaModel *cosine = new CosineAntennaModel();
	node->setCosineAntennaModel(cosine);

	// Set Receive ok callback
	node->SetReceiveOkCallback (MakeCallback (&NodeND::Receive, this));

	m_node = node;

	return node;
}

// Run
void NodeND::Run(){

}

} /* namespace ns3 */
