/*
 * adaptiveND.h
 *
 *  Created on: Oct 28, 2015
 *      Author: johannes
 */

#ifndef ADAPTIVEND_H_
#define ADAPTIVEND_H_

namespace ns3 {

class AdaptiveND {
public:
	AdaptiveND();
	virtual ~AdaptiveND();
};

} /* namespace ns3 */

#endif /* ADAPTIVEND_H_ */
