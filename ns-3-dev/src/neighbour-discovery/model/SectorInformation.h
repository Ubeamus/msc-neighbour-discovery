/*
 * SectorInformation.h
 *
 *  Created on: Dec 16, 2015
 *      Author: johannes
 */

#ifndef SECTORINFORMATION_H_
#define SECTORINFORMATION_H_

 #include <stdint.h>

struct SectorInformation
{
	uint16_t id;
	double direction;
	double beamwidth;
};

#endif /* SECTORINFORMATION_H_ */
