/*
 * tree.cc
 *
 *  Created on: Dec 16, 2015
 *      Author: johannes
 */

#include "Tree.h"
#include "ns3/log.h"
#include <math.h>
#include <stdlib.h>     /* srand, rand */
#include <stdio.h>      /* printf, NULL */
#include <time.h>       /* time */
#include <sstream>		/*for std_to_string bug*/


// Patch for std::to_string
namespace patch
{
    template < typename T > std::string to_string( const T& n )
    {
        std::ostringstream stm ;
        stm << n ;
        return stm.str() ;
    }
}


namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("Tree");

Tree::Tree() {
	// TODO Auto-generated constructor stub
	antennaDirections = 360;
	sharedChildrenProbabilityValue = 0.6666666;

	// Probability Vallues
	minSectorProbabilityValue = 0.1;
	maxSectorProbabilityValue = 0.8;

	minChildProbabilityValue = 0.1;
	maxChildProbabilityValue = 0.8;

}

Tree::~Tree() {
	// TODO Auto-generated destructor stub
}

/*
 * Creates an n-tree that holds sectors at each layer with a certain
 * beamwidth and a specified direction
 *
 * calls initializeProbabilityValues
 */

void Tree::createTree(	std::vector<uint16_t> childrenPerNode,
						std::vector<double> beamwidthPerLayer,
						std::vector<double> offset,
						int offsetDirection){

	NS_LOG_FUNCTION("Tree::createTree");

	int id = 0;

	// Root node id
	rootNode.id = id;
	rootNode.parent = NULL;

	// Current sector nodes
	std::vector<SectorNew *> currentSectorLayer;

	// Set current layer level
	currentSectorLayer.push_back(&rootNode);

	for(uint16_t i = 0; i < childrenPerNode.size() ; i++){

		int sectorPerLayer = 1;
		for (int k=0; k <= i; k++) { // calculate nodes per layer from children per node
			sectorPerLayer *= childrenPerNode.at(k);
			NS_LOG_DEBUG("SectorPerLayer " << sectorPerLayer);
		}

		// Sectors per layer
		for(uint16_t j = 0; j < sectorPerLayer; j++){
			SectorNew *treeSector = new SectorNew();

			// Create Sector Information
			treeSector->beamwidth = beamwidthPerLayer.at(i);

			// Calculate direction for sector
			if(i == 0){ // first sector layer
				treeSector->direction = ((offsetDirection + ((antennaDirections / sectorPerLayer) * j)) % 360);
			}

			id++;
			treeSector->id = id;

			NS_LOG_DEBUG("--- NEW NODE ID " << id << " Beamwidth " << treeSector->beamwidth << " Direction " << treeSector->direction);

			bool added = false;

			while(!added){

				for(uint16_t k = 0; k < currentSectorLayer.size(); k++){
					NS_LOG_DEBUG("currentSectorLayer.size() " << currentSectorLayer.size());
					if(!added){
						if((int)currentSectorLayer.at(k)->children.size() < childrenPerNode.at(i)){
							NS_LOG_DEBUG("rootNode k "<< k << " id " << currentSectorLayer.at(k)->id);
							currentSectorLayer.at(k)->children.push_back(treeSector);
							treeSector->parent = currentSectorLayer.at(k); // currentSectorLayer.at(k); // set parent link

							// Calculate direction for sector depending on parent direction
							if(i != 0){ // not first layer

								double parentSector = treeSector->parent->direction / (360/antennaDirections);
								NS_LOG_DEBUG("Parent sector " << parentSector << " parent id " << treeSector->parent->id);

								double calculatedOffset = 0;

								for (int h = 0; h < (int) treeSector->parent->children.size(); ++h) {
									if(h == 0){
										calculatedOffset = offset.at(i); // offset for layer
									} else if (h % 2) {
										calculatedOffset *= -1;
									} else {
										calculatedOffset *= -2;
									}
								}
								NS_LOG_DEBUG("CalculatedOffset " << calculatedOffset);

								// mod antennaSectors and + if negative
								double calulatedDirection = fmod((parentSector + calculatedOffset), antennaDirections);
								if(calulatedDirection < 0){
									calulatedDirection = antennaDirections + calulatedDirection; // + since calculatedDirection is negative
								}
								NS_LOG_DEBUG("CalculatedOffset " << calulatedDirection << " treeNode " << treeSector->id);
								NS_LOG_DEBUG("Set Direction " << (calulatedDirection) * (360/antennaDirections));
								treeSector->direction = (calulatedDirection) * (360/antennaDirections); // 360/antennaSectors converts sec to direc

							}

							NS_LOG_DEBUG("ADDED IS TRUE");
							added = true;
						}
					}
				}

				if(!added){ // children of children
					NS_LOG_DEBUG("Set children of children");
					std::vector<SectorNew *> tmp = currentSectorLayer;
					currentSectorLayer.empty();
					for(uint16_t z = 0; z < tmp.size(); z++){
						for(uint16_t y = 0; y < tmp.at(z)->children.size(); y++){
							currentSectorLayer.push_back(tmp.at(z)->children.at(y));
						}
					}
				}
			}
		}
	}

	initializeProbabilityValues();

}

/*
 * Adds probability values to the nodes so that each node keeps a value
 * 1 - p1 - p2 - ... -pn for its own and the p1 ... pn values for its children
 */
void Tree::initializeProbabilityValues(){
	NS_LOG_FUNCTION("Tree::initializeProbabilityValues");

	std::vector<SectorNew* > tmpNodes;
	tmpNodes.push_back(&rootNode);

	bool run = true;

	while(run){

		// Print children node info
		for(uint16_t i = 0; i < tmpNodes.size(); i++){

			int numberChildren = tmpNodes.at(i)->children.size();

			for(uint16_t j = 0; j < numberChildren + 1 ; j++){ // children + parent

				if(tmpNodes.at(i)->id == 0){ // root
					if(j == 0){
						// skip probability, since root node
					} else {
						tmpNodes.at(i)->probabilities.push_back(1.0/numberChildren);
					}
				}

				if(tmpNodes.at(i)->children.size() > 0 && tmpNodes.at(i)->id != 0){ // middle nodes
					if(tmpNodes.at(i)->probabilities.size() == 0){
						tmpNodes.at(i)->probabilities.push_back(1-sharedChildrenProbabilityValue);
					} else {
						tmpNodes.at(i)->probabilities.push_back(sharedChildrenProbabilityValue/numberChildren);
					}
				}

				if(tmpNodes.at(i)->children.size() == 0 && tmpNodes.at(i)->id != 0){ // leaf nodes
					tmpNodes.at(i)->probabilities.push_back(1);
				}
			}
		}

		// Set new children
		std::vector<SectorNew *> tmp = tmpNodes;
		tmpNodes.clear();
		for(uint16_t z = 0; z < tmp.size(); z++){
			for(uint16_t y = 0; y < tmp.at(z)->children.size(); y++){
				tmpNodes.push_back(tmp.at(z)->children.at(y));
			}
		}

		// If no children available break
		if(tmpNodes.size() == 0){
			run = false;
		}
	}

}

/*
 * Prints the structure of the tree to the console
 */
void Tree::printTree(){
	NS_LOG_FUNCTION("Tree::printTree");
	NS_LOG_UNCOND("----------------------------------- printTree --------------------------------------");

	std::vector<SectorNew* > tmpNodes;
	tmpNodes.push_back(&rootNode);

	// NS_LOG_UNCOND("rootNode dsalfjalsf size " << rootNode.children.size());

	bool run = true;

	while(run){

		// Print children node info
		// NS_LOG_UNCOND("Tmp Nodes size " << tmpNodes.size());
		for(uint16_t i = 0; i < tmpNodes.size(); i++){
			if(tmpNodes.at(i)->parent != NULL){
				NS_LOG_UNCOND("Sector Id " << tmpNodes.at(i)->id << " beamwidth " << tmpNodes.at(i)->beamwidth
						<< " direction " << tmpNodes.at(i)->direction << " Parent node id " << tmpNodes.at(i)->parent->id);
			} else {
				NS_LOG_UNCOND("Sector Id " << tmpNodes.at(i)->id << " beamwidth " << tmpNodes.at(i)->beamwidth
						<< " direction " << tmpNodes.at(i)->direction << " Parent node id ");
			}
		}

		// Print probability values
		std::string str;

		for(uint16_t i = 0; i < tmpNodes.size(); i++){
			for(uint16_t j = 0; j < tmpNodes.at(i)->probabilities.size(); j++){

				str += " / ";
				str += patch::to_string(tmpNodes.at(i)->probabilities.at(j));
			}
			NS_LOG_UNCOND("Probability " << str);
			str = "";
		}

		// Set new children
		std::vector<SectorNew *> tmp = tmpNodes;
		tmpNodes.clear();
		for(uint16_t z = 0; z < tmp.size(); z++){
			for(uint16_t y = 0; y < tmp.at(z)->children.size(); y++){
				tmpNodes.push_back(tmp.at(z)->children.at(y));
			}
		}

		// If no children available break
		if(tmpNodes.size() == 0){
			run = false;
		}
	}

	NS_LOG_UNCOND("----------------------------------- END printTree --------------------------------------");
}

/*
 * Returns a sector from the tree
 */

SectorNew* Tree::getMostProbableSector(){
	NS_LOG_FUNCTION("Tree::getMostProbableSector");

	SectorNew* tmpNode = NULL; // Tmp sector

	tmpNode = &rootNode;

	bool oldSameNew = false;
	bool setNewSector = false;

	while(!oldSameNew){

		double rand = fRand(0.0, 1.0); // choose sector
		double prob = 0;				// accumulated probability values

		// NS_LOG_UNCOND("probability value " << rand);

		uint16_t chosenSectorIndex = 0;

		// Chose sector
		for(uint16_t i = 0; i < tmpNode->probabilities.size(); i++){ // Root node
			prob = prob + tmpNode->probabilities.at(i);

			// Check sectors underneath root sector
			if(tmpNode->id == 0 && !setNewSector){ // Exception for root node
				if(rand <= prob){
					chosenSectorIndex = i;
					NS_LOG_INFO("root chosen sector " << chosenSectorIndex);
					setNewSector = true;
				}
			}

			// Check sectors underneath sector
			if(!setNewSector){
				if(rand <= prob){
					chosenSectorIndex = i;
					NS_LOG_INFO("chosen sector " << chosenSectorIndex);
					setNewSector = true;
				}
			}
		}

		if(tmpNode->id == 0 && setNewSector){
			tmpNode = tmpNode->children.at(chosenSectorIndex); // set child sector
			setNewSector = false;
		} else if (setNewSector){
			if(chosenSectorIndex == 0){
				oldSameNew = true; // same sector again
			} else {
				tmpNode = tmpNode->children.at(chosenSectorIndex-1); // set child sector
				setNewSector = false;
			}
		}
	}


	// NS_LOG_UNCOND ("Chosen sector " << tmpNode->id);

	return tmpNode;
}


SectorNew* Tree::getSector(int sectorId){
	NS_LOG_FUNCTION("Tree::getSector (sectorId) = " << sectorId);

	// printTree();

	std::vector<SectorNew* > tmpNodes;
	tmpNodes.push_back(&rootNode);

	bool run = true;

	while(run){

		// Print children node info
		for(uint16_t i = 0; i < tmpNodes.size(); i++){
			for(uint16_t j = 0; j < tmpNodes.at(i)->children.size(); j++){
				NS_LOG_DEBUG("Child " << tmpNodes.at(i)->children.at(j)->id);
				NS_LOG_DEBUG("tmp.nodes.at(i) "  << tmpNodes.at(i)->id << " sectorId " << sectorId);


				if(tmpNodes.at(i)->children.at(j)->id == sectorId){ // return Sector* if found
					NS_LOG_DEBUG("Found sector ");
					return tmpNodes.at(i)->children.at(j);
				}
			}
		}

		// Set new children
		std::vector<SectorNew *> tmp = tmpNodes;
		tmpNodes.clear();
		for(uint16_t z = 0; z < tmp.size(); z++){
			for(uint16_t y = 0; y < tmp.at(z)->children.size(); y++){
				tmpNodes.push_back(tmp.at(z)->children.at(y));
			}
		}

		// If no children available break
		if(tmpNodes.size() == 0){
			NS_LOG_DEBUG("Empty ");
			run = false;
		}
	}

	SectorNew *empty = NULL;
	return empty;

}

// Logig zum erhöhen und verringern der werte in methoden auslagern und nur noch diese aufrufen ...

void Tree::updateTransmitProbability(Sector *sector){
	NS_LOG_FUNCTION("Tree::updateTransmitProbability - parameter sector id " << sector->id);

	// decrease value of sector
	double value = 0.01;

	if(sector->id != 0 && sector->children.size() > 0){ // middle node

		double secProb = sector->probabilities.at(0); // sector prob.

		if(secProb > 0.1){ // if > 0.1 decrease else do nothing
			double newValue =  sector->probabilities.at(0) - value;
			sector->probabilities.at(0) = newValue;

			// Increase probability of child nodes by 0.1/#child
			for(int i = 1; i < (int)sector->probabilities.size(); i++){
				newValue = sector->probabilities.at(i) + value / (sector->probabilities.size()-1);
				sector->probabilities.at(i) = newValue;
			}
		} else {
			// prob. already low enough
		}
	} else if(sector->id != 0 && sector->children.size() == 0){ // leaf node
		Sector *parent = sector->parent;

		int indexPos = -1;
		for (int i = 0; i < (int)parent->children.size(); i++) { // find index of sector in parents children vector
			if(sector->id == parent->children.at(i).id){
				indexPos = i;
				NS_LOG_DEBUG("Pos " << indexPos);

				if(parent->id != 0){ // if parent is not root probability value indexPos + 1
					double secProb = parent->probabilities.at(indexPos+1);

					if(secProb > 0.1){
						double newValue =  parent->probabilities.at(indexPos+1) - value;
						parent->probabilities.at(indexPos+1) = newValue;

						// Increase probability of parent and other child nodes by 0.1/(#child-1+#parent)
						for(int i = 0; i < (int)parent->probabilities.size(); i++){
							if(i != (indexPos+1)){
								newValue = parent->probabilities.at(i) + value / (parent->probabilities.size() - 1);
								parent->probabilities.at(i) = newValue;
							}
						}
					}
				} else { // if parent is root probability value indexPos

				}

				Sector tmp_Parent = parent->children.at(indexPos);
				NS_LOG_DEBUG("parent id " << tmp_Parent.id);
			}
		}

	} else { // root node

	}
}

void Tree::updateSector(SectorNew *sector, double value){
	// Function log
	NS_LOG_FUNCTION("Tree::updateSectorIncrease - parameter sector id " << sector->id << " value "  << value);

	// If sector is root do nothing, root node cannot be used to transmit or receive data
	if(sector->id == 0){
		return;
	}

	// If sector is MIDDLE NODE, increase sector probability and decrease childrens probability
	if (sector->id != 0 && sector->children.size() > 0){

		NS_LOG_DEBUG("Middle node");

		// Probability value of the MIDDLE NODE
		double sectorProb = sector->probabilities.at(0);

		// Check if probability value is after change still smaller than maxSectorProbabilityValue
		if((sectorProb+value) < maxSectorProbabilityValue && (sectorProb+value) > minSectorProbabilityValue){

			// Add value to sector
			sector->probabilities.at(0) = sector->probabilities.at(0) + value;

			proportionalUpdateSector(value, sector);

			// Split value through the number of children nodes and sub it from the probablitiy value
			//for(int i = 1; i < (int)sector->probabilities.size(); i++){ // from 1 to exclude sector
			//	sector->probabilities.at(i) = sector->probabilities.at(i) - value / (sector->probabilities.size()-1);
			//}
		} else {
			// Probability already high, do nothing
		}
	}
}

void Tree::updateParent(SectorNew *sector, double value){
	// Function log
	NS_LOG_FUNCTION("Tree::updateSectorIncrease - parameter sector id " << sector->id << " value "  << value);

	// If sector is root do nothing, root node cannot be used to transmit or receive data
	if(sector->id == 0){
		return;
	}

	// Parent node
	SectorNew *parent = sector->parent;

	// Index of child node in parents node list
	int indexPos = -1;

	// Find index of sector in parents children vector
	for (int i = 0; i < (int)parent->children.size(); i++) {

		// Check if id of sector and children of parent is same -> index found
		if(sector->id == parent->children.at(i)->id){
			indexPos = i;

			// --- If parent is a MIDDLE NODE
			if(parent->id != 0){

				NS_LOG_DEBUG("Parent is not root");

				// Probability value is indexPos+1, since parent node itself is in probability list as well
				double sectorProb = parent->probabilities.at(indexPos+1);
				NS_LOG_DEBUG("SectorProbability " << sectorProb);

				if((sectorProb+value) < maxChildProbabilityValue && (sectorProb+value) > minChildProbabilityValue){

					// Update parent
					parent->probabilities.at(indexPos+1) = parent->probabilities.at(indexPos+1) + value;
					NS_LOG_DEBUG("New SectorProbability " << parent->probabilities.at(indexPos+1));

					// Update children of parent
					proportionalUpdateParent(indexPos, value, parent);

				}

			// --- If parent is the ROOT NODE
			} else {

				// Probability value is indexPos, since only children nodes are within the list
				double sectorProb = parent->probabilities.at(indexPos);

				// Check if value is still smaller than maxChildProbabilityValue
				if((sectorProb+value) < maxChildProbabilityValue && (sectorProb+value) > minChildProbabilityValue){

					// Add value to sectors probability value
					parent->probabilities.at(indexPos) = parent->probabilities.at(indexPos) + value;

					// Decrease probability of other child nodes by value/(#child-1+#parent)
					for(int i = 0; i < (int)parent->probabilities.size(); i++){

						// Do not decrease value of children node we increased the value of
						if(i != (indexPos)){
							parent->probabilities.at(i) = parent->probabilities.at(i) - value / (parent->probabilities.size() - 1);
						}
					}
				}
			}
		}
	}
}

void Tree::recursiveUpdateSector(SectorNew *sector, double value){

	SectorNew* tmp = sector;

	while(tmp->id != 0){
		updateSector(tmp, value);
		tmp = tmp->parent;
	}
}

void Tree::recursiveUpdateParent(SectorNew *sector, double value){
	SectorNew* tmp = sector;

	while(tmp->id != 0){
		updateParent(tmp, value);
		tmp = tmp->parent;
	}
}

void Tree::resetProbabilityValue(SectorNew *sector, double value){
	for(int i=0; i < (int) sector->probabilities.size(); i++){
		if(i == 0){
			sector->probabilities.at(0) = value;
		} else {
			sector->probabilities.at(i) = (1 - value) / (sector->probabilities.size() - 1);
		}
	}
}

std::vector<double> Tree::normalize(std::vector<double> valuesToNormalize){

	double normalizeIndex = 0;

	for(int i=0; i < (int)valuesToNormalize.size(); i++){
		NS_LOG_DEBUG("valuesToNormliaze " << valuesToNormalize.at(i));
		normalizeIndex += valuesToNormalize.at(i);
	}

	NS_LOG_DEBUG("normliaze Index " << normalizeIndex);

	std::vector<double> normalizedVector;

	for(int i=0; i < (int) valuesToNormalize.size(); i++){
		NS_LOG_DEBUG("valuesToNormliaze.at(i) " << valuesToNormalize.at(i) << " normalize index " << normalizeIndex);
		NS_LOG_DEBUG("new value " << valuesToNormalize.at(i) / normalizeIndex);

		normalizedVector.push_back(valuesToNormalize.at(i) / normalizeIndex);
	}

	return normalizedVector;
}

void Tree::proportionalUpdateParent(int indexPos, double value, SectorNew *parent){
	std::vector<double> toNormalizeProbabilityValues;

	for(int i = 0; i < (int)parent->probabilities.size(); i++){
		if(i != (indexPos+1)){
			toNormalizeProbabilityValues.push_back(parent->probabilities.at(i));
		}
	}

	NS_LOG_DEBUG("Probability values");
	for(int i = 0; i < (int)parent->probabilities.size(); i++){
		NS_LOG_DEBUG("Probability values at " << i << " "<< parent->probabilities.at(i));
	}

	toNormalizeProbabilityValues = normalize(toNormalizeProbabilityValues);

	int normalizeIndex = 0;
	for(int i = 0; i < (int)parent->probabilities.size(); i++){

		if(i != (indexPos+1)){ // for all other nodes
			NS_LOG_DEBUG("Value " << value << " normalizeValue " << toNormalizeProbabilityValues.at(normalizeIndex) <<  " toNormalizeProbabilityValues " << value * toNormalizeProbabilityValues.at(normalizeIndex));

			if(parent->probabilities.at(i) - value * toNormalizeProbabilityValues.at(normalizeIndex) > minChildProbabilityValue &&
			   parent->probabilities.at(i) - value * toNormalizeProbabilityValues.at(normalizeIndex) < maxChildProbabilityValue){

				NS_LOG_DEBUG("Index " << i << " oldValue " << parent->probabilities.at(i));

				parent->probabilities.at(i) = parent->probabilities.at(i) - value * toNormalizeProbabilityValues.at(normalizeIndex);
				NS_LOG_DEBUG("Index " << i << " newValue " << parent->probabilities.at(i));
				normalizeIndex++;

			} else {
				NS_LOG_DEBUG("return value to parent - proportionalUpdate");
				NS_LOG_DEBUG("Old Value parent " << parent->probabilities.at(i));
				parent->probabilities.at(indexPos+1) = parent->probabilities.at(indexPos+1) - value * toNormalizeProbabilityValues.at(normalizeIndex);
				NS_LOG_DEBUG("New Value parent " << parent->probabilities.at(i));
				normalizeIndex++;
			}
		}
		NS_LOG_DEBUG("Probability values");
		for(int i = 0; i < (int)parent->probabilities.size(); i++){
			NS_LOG_DEBUG("Probability values at " << i << " "<< parent->probabilities.at(i));
		}
	}
}

void Tree::proportionalUpdateSector(double value, SectorNew *sector){

	std::vector<double> toNormalizeProbabilityValues;

	for(int i = 1; i < (int)sector->probabilities.size(); i++){
		toNormalizeProbabilityValues.push_back(sector->probabilities.at(i));
	}

	toNormalizeProbabilityValues = normalize(toNormalizeProbabilityValues);

	int normalizeIndex = 0;

	for(int i = 1; i < (int)sector->probabilities.size(); i++){
		NS_LOG_DEBUG("Value " << value << " normalizeValue " <<  toNormalizeProbabilityValues.at(normalizeIndex) << " toNormalizeProbabilityValues " << value * toNormalizeProbabilityValues.at(normalizeIndex));

		if(sector->probabilities.at(i) - value * toNormalizeProbabilityValues.at(normalizeIndex) > minChildProbabilityValue &&
		   sector->probabilities.at(i) - value * toNormalizeProbabilityValues.at(normalizeIndex) < maxChildProbabilityValue){

			sector->probabilities.at(i) = sector->probabilities.at(i) - value * toNormalizeProbabilityValues.at(normalizeIndex);
			normalizeIndex++;

		} else {
			// return value to parent
			NS_LOG_DEBUG("return value to parent");
			sector->probabilities.at(0) = sector->probabilities.at(0) - value * toNormalizeProbabilityValues.at(normalizeIndex);
			normalizeIndex++;
		}

		NS_LOG_DEBUG("Probability values");
		for(int i = 0; i < (int)sector->probabilities.size(); i++){
			NS_LOG_DEBUG("Probability values at " << i << " "<< sector->probabilities.at(i));
		}
	}
}


// Helper
double
Tree::fRand(double fMin, double fMax)
{
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

} /* namespace ns3 */
