/*
 * node.h
 *
 *  Created on: Dec 21, 2015
 *      Author: johannes
 */

#ifndef SECTOR_H_
#define SECTOR_H_

#include <vector>
#include <stdint.h>

namespace ns3 {

class Sector {
public:
	Sector();
	virtual ~Sector();

	uint16_t id;

	Sector *parent;

	// Node
	double beamwidth;
	double direction;

	// Children
	std::vector<Sector> children; // child 1, child 2, ... , child n
	std::vector<double> probabilities; // node, p(child 1), p(child 2), ... , p(child n)

private:

};

} /* namespace ns3 */

#endif /* SECTOR_H_ */
