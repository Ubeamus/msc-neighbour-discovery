/*
 * sweepND.h
 *
 *  Created on: Oct 28, 2015
 *      Author: johannes
 */

#ifndef SWEEPND_H_
#define SWEEPND_H_

#include "ns3/neighbour-discovery.h"

namespace ns3 {

class SweepND {

public:
	SweepND();
	virtual ~SweepND();

	void startSweepND();
	void stopSweepND();

	void setNeighbourDiscoveryList(std::list<NeighbourDiscovery> list);

	// Getter
	uint16_t const& getNumNodes() const;
	uint16_t const& getAreaLength() const;
	uint16_t const& getAreaWidth() const;

	// Setter
	void setNumNodes(uint16_t const& newNumNodes);
	void setAreaLength(uint16_t const& newNumNodes);
	void setAreaWidth(uint16_t const& newNumNodes);

private:

	uint16_t m_numNodes;
	uint16_t m_areaLength;
	uint16_t m_areaWidth;

	std::list<NeighbourDiscovery> m_nodesList;
};

} /* namespace ns3 */

#endif /* SWEEPND_H_ */
