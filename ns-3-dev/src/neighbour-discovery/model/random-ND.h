/*
 * random-ND.h
 *
 *  Created on: Oct 28, 2015
 *      Author: johannes
 */

#ifndef RANDOMND_H_
#define RANDOMND_H_

namespace ns3 {

class RandomND {

public:
	RandomND();
	virtual ~RandomND();

	Vector randomPlaceNodes();
	int setNewOrientation();
	void sendBeacon();
	void receiveBeacon();

	// Getter
	int const& getNumNodes() const;
	int const& getAreaLength() const;
	int const& getAreaWidth() const;

	// Setter
	void setNumNodes(int const& newNumNodes);
	void setAreaLength(int const& newNumNodes);
	void setAreaWidth(int const& newNumNodes);

private:

	int numNodes;
	int areaLength;
	int areaWidth;

};

} /* namespace ns3 */

#endif /* RANDOMND_H_ */
